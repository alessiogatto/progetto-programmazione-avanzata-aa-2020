# Guida al deploy

La web app presente in questo repository è stata realizzata tramite Java, gestito con Maven.
Tale guida al deploy è valida per il SO Ubuntu.

Il deploy avviene in un server Tomcat locale ma è necessario eseguire questi passaggi preliminari:

- Installare MySQL 8. L'utente e la password di default del progetto sono *Username: root* e *Password: P@ssw0rd*.
    È possibile cambiare tale configurazione alle righe 31 e 32 del file DataServiceConfig.java presente nella cartella src/main/java/it/univpm/advprog/delivery/app/

Per installare MySQL, è necessario eseguire:
```console
sudo apt install mysql-server
```
La configurazione avviene tramite:
```console
sudo mysql_secure_installation
```
- Installare Tomcat 9. È possibile scaricarlo dal sito ufficiale o dal repository del sistema operativo (consigliato).
  L'utente configurato per il deploy ha *Username: admin* e *Password: admin*. È possibile cambiare tale configurazione alle righe 34 e 35 del pom.xml.
  
Per installare Tomcat, è necessario eseguire:
```console
sudo apt install tomcat9 tomca9-admin tomcat9-docs tomcat9-examples
```
Verificare la corretta installazione tramite il comando:
```console
sudo systemctl status tomcat9
```
Qualora il demone non fosse attivo, eseguire il comando:
```console
sudo systemctl start tomcat9
```
- Installare OpenJDK e Maven
```console
sudo apt install default-jdk maven
```
- Tramite un IDE come Eclipse o Intellij, importare il progetto ed eseguire il sorgente Java LoadDataTest.java presente nella cartella src/main/java/it/univpm/advprog/delivery/test. Questo passaggio permette di creare un database di prova
 dove sono contenuti alcuni articoli di esempio. Sono presenti, inoltre, un utente generico con *Username: user* e *Password: user* e un utente amministratore con *Username: admin* e *Password: admin*.
  

Una volta che sono stati eseguiti questi passaggi, bisogna aprire un terminale e posizionarsi nella root del progetto. Eseguire, quindi, il comando
```console
mvn tomcat7:deploy
```
Maven scaricherà le dipendenze presenti nel pom ed eseguirà i test prima di effettuare il deploy. Il progetto verrà deployato all'url http://localhost:8080/delivery/

La web app è stata testata su Fedora 33, Ubuntu 20.04.1 con MySQL 8.0.22, Tomcat 9.0.40/9.0.41 e OpenJDK 11 e 15.0.1. Il funzionamento è stato verificato sul browser Chrome, potrebbero verificarsi dei comportanti indesiderati su altri browser.

NB: il file di configurazione per gli utenti di Tomcat è in /etc/tomcat9/tomcat-users.xml. In caso di problemi è possibile utilizzare il file di configurazione presente in questo repository.
### AUTORI
- Alborino Francesco
- Gatto Alessio
- Mahdavidaronkola Human
