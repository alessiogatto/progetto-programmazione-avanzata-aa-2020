package it.univpm.advprog.delivery.test.unit;

import it.univpm.advprog.delivery.model.dao.*;
import it.univpm.advprog.delivery.model.entities.Address;
import it.univpm.advprog.delivery.model.entities.Article;
import it.univpm.advprog.delivery.model.entities.Order;
import it.univpm.advprog.delivery.model.entities.User;
import it.univpm.advprog.delivery.service.AddressService;
import it.univpm.advprog.delivery.service.OrderService;
import it.univpm.advprog.delivery.test.DataServiceConfigTest;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.Calendar;
import java.util.TimeZone;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestOrder {

    private static AnnotationConfigApplicationContext ctx;

    @BeforeAll
    static void setUp() {
        System.out.println("Prepare the test environment");
        ctx = new AnnotationConfigApplicationContext(DataServiceConfigTest.class);
    }

    @AfterAll
    static void tearDown() {
        ctx.close();
    }

    @Test
    public void testCreateOrder() {
            SessionFactory sf = ctx.getBean("sessionFactory", SessionFactory.class);
            AddressDao addressDao = ctx.getBean("addressDao", AddressDao.class);
            ArticleDao articleDao = ctx.getBean("articleDao", ArticleDao.class);
            CartDao cartDao = ctx.getBean("cartDao", CartDao.class);
            UserDao userDao = ctx.getBean("userDao", UserDao.class);
            OrderDao orderDao = ctx.getBean("orderDao", OrderDao.class);
            AddressService addressService = ctx.getBean("addressService", AddressService.class);
            OrderService orderService = ctx.getBean("orderService", OrderService.class);

            Session s = sf.openSession();
            s.beginTransaction();
            addressDao.setSession(s);
            articleDao.setSession(s);
            cartDao.setSession(s);
            userDao.setSession(s);
            orderDao.setSession(s);

            assertEquals(s, userDao.getSession());
            Address address = addressDao.create("via roma", "12C", 60100, "roma", Address.typeAddress.LAVORO);
            User user = userDao.create("usernameTestCreateOrder", userDao.encryptPassword("password"), "firstname", "lastname", "ciao@ciao.ci", User.typeRole.UTENTE, "1234567");
            addressService.linkAddress(user.getUsername(), address.getIdAddress());
            Article article = articleDao.create("pizza", 5.69, "prova descrizione", "http://", "farina, ecc", Article.typeArticle.PIZZE, Article.typeDieta.PER_TUTTI, 22, true);
            Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC")); //istanza della data e ora corrente
            Order order = orderDao.create(Order.typeOrder.DELIVERY, user, Order.payMethodOrder.CARTA_DI_CREDITO, calendar, calendar, "prova", address.getIdAddress());
            orderService.addToCart(order,article, 2);
            s.getTransaction().commit();
            assertEquals(11.38, order.getTotalAmountOrder());

    }
}
