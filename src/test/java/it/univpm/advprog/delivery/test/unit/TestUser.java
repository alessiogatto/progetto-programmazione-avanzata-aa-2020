package it.univpm.advprog.delivery.test.unit;

import it.univpm.advprog.delivery.model.dao.AddressDao;
import it.univpm.advprog.delivery.model.dao.UserDao;
import it.univpm.advprog.delivery.model.entities.Address;
import it.univpm.advprog.delivery.model.entities.User;
import it.univpm.advprog.delivery.service.AddressService;
import it.univpm.advprog.delivery.test.DataServiceConfigTest;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class TestUser {

    private static AnnotationConfigApplicationContext ctx;

    @BeforeAll
    static void setUp() {
        System.out.println("Prepare the test environment");
        ctx = new AnnotationConfigApplicationContext(DataServiceConfigTest.class);
    }

    @AfterAll
    static void tearDown() {
        ctx.close();
    }
    @Test
    public void testInsert() {
        try (AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(DataServiceConfigTest.class)) {
            SessionFactory sf = ctx.getBean("sessionFactory", SessionFactory.class);
            AddressDao addressDao = ctx.getBean("addressDao", AddressDao.class);
            UserDao userDao = ctx.getBean("userDao", UserDao.class);
            AddressService addressService = ctx.getBean("addressService", AddressService.class);
            Session s = sf.openSession();
            s.beginTransaction();
            addressDao.setSession(s);
            userDao.setSession(s);
            assertEquals(s, userDao.getSession());
            int number = 15;
            for (int i = 0; i < number; i++) {
                Address address = addressDao.create("via roma" + i, "12C" + i, 60100, "roma" + i, Address.typeAddress.CASA);
                User user = userDao.create("username" + i, userDao.encryptPassword("password" + i), "firstname", "lastname", "ciao@ciao.ci" + i, User.typeRole.UTENTE, "1234567");
                addressService.linkAddress(user.getUsername(), address.getIdAddress());

            }
            s.getTransaction().commit();
            assertEquals(number, userDao.findAll().size());
        }
    }
}
