package it.univpm.advprog.delivery.test.unit;

import it.univpm.advprog.delivery.model.dao.ArticleDao;
import it.univpm.advprog.delivery.model.entities.Article;
import it.univpm.advprog.delivery.test.DataServiceConfigTest;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import static org.junit.jupiter.api.Assertions.*;


public class TestArticle {

    private static AnnotationConfigApplicationContext ctx;

    @BeforeAll
    static void setUp() {
        System.out.println("Prepare the test environment");
        ctx = new AnnotationConfigApplicationContext(DataServiceConfigTest.class);
    }

    @AfterAll
    static void tearDown() {
        ctx.close();
    }

    @Test
    public void testBeginCommitTransactionAndCreateArticle() {
        SessionFactory sf = ctx.getBean("sessionFactory", SessionFactory.class);
        ArticleDao articleDao = ctx.getBean("articleDao", ArticleDao.class);
        Session s = sf.openSession();
        assertTrue(s.isOpen());
        s.beginTransaction();
        articleDao.setSession(s);
        assertEquals(s, articleDao.getSession());
        Article article = articleDao.create("pizza", 5.00, "prova descrizione", "http://", "farina, ecc", Article.typeArticle.PIZZE, Article.typeDieta.PER_TUTTI, 22, true);
        s.getTransaction().commit();
        s.beginTransaction();
        article.setDescriptionArticle("nuova descrizione");
        article = articleDao.update(article);
        s.getTransaction().commit();
        assertEquals("nuova descrizione", article.getDescriptionArticle());
        assertFalse(s.getTransaction().isActive());
    }
}
