package it.univpm.advprog.delivery.test.unit;

import it.univpm.advprog.delivery.model.dao.AddressDao;
import it.univpm.advprog.delivery.model.dao.UserDao;
import it.univpm.advprog.delivery.model.entities.Address;
import it.univpm.advprog.delivery.model.entities.User;
import it.univpm.advprog.delivery.service.AddressService;
import it.univpm.advprog.delivery.test.DataServiceConfigTest;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import static org.junit.jupiter.api.Assertions.*;

public class TestAddressDao {

    private static AnnotationConfigApplicationContext ctx;

    @BeforeAll
    static void setUp() {
        System.out.println("Prepare the test environment");
        ctx = new AnnotationConfigApplicationContext(DataServiceConfigTest.class);
    }

    @AfterAll
    static void tearDown() {
        ctx.close();
    }

    @Test
    public void testBeginCommitTransaction() {
            SessionFactory sf = ctx.getBean("sessionFactory", SessionFactory.class);
            AddressDao addressDao = ctx.getBean("addressDao", AddressDao.class);
            Session s = sf.openSession();
            assertTrue(s.isOpen());
            s.beginTransaction();
            addressDao.setSession(s);
            assertEquals(s, addressDao.getSession());
            s.getTransaction().commit();
            assertFalse(s.getTransaction().isActive());
    }

    @Test
    public void testInsertAndFindByIdAddress() {
            SessionFactory sf = ctx.getBean("sessionFactory", SessionFactory.class);
            AddressDao addressDao = ctx.getBean("addressDao", AddressDao.class);
            Session s = sf.openSession();
            s.beginTransaction();
            addressDao.setSession(s);
            Address address = addressDao.create("via roma", "12C", 123456789, "roma", Address.typeAddress.PREFERITO);
            s.getTransaction().commit();
            int id = address.getIdAddress();
            assertEquals(address, addressDao.findByIdAddress(id));
            assertEquals(address.getCapAddress(), 12345); // substring del cap, prime 5 cifre
    }

    @Test
    public void testInsertAndDeleteAddress() {
            SessionFactory sf = ctx.getBean("sessionFactory", SessionFactory.class);
            AddressDao addressDao = ctx.getBean("addressDao", AddressDao.class);
            Session s = sf.openSession();
            s.beginTransaction();
            addressDao.setSession(s);
            Address address = addressDao.create("via roma", "12C", 00100, "roma", Address.typeAddress.CASA);
            int id = address.getIdAddress();
            s.getTransaction().commit();
            s.beginTransaction();
            addressDao.delete(address);
            s.getTransaction().commit();
            assertNull(addressDao.findByIdAddress(id));
    }
    @Test
    public void testLinkUnlink() {
            SessionFactory sf = ctx.getBean("sessionFactory", SessionFactory.class);
            AddressDao addressDao = ctx.getBean("addressDao", AddressDao.class);
            UserDao userDao = ctx.getBean("userDao", UserDao.class);
            AddressService addressService = ctx.getBean("addressService", AddressService.class);
            Session s = sf.openSession();
            s.beginTransaction();
            addressDao.setSession(s);
            userDao.setSession(s);
            Address address = addressDao.create("via roma", "12C", 60100, "roma", Address.typeAddress.CASA);
            s.getTransaction().commit();
            s.beginTransaction();
            String password = userDao.encryptPassword("password");
            User user = userDao.create("nome", password, "nome", "cognome", "prova@pr.pr", User.typeRole.UTENTE, "12345678");
            addressService.linkAddress(user.getUsername(), address.getIdAddress());
            s.getTransaction().commit();
            assertEquals(address, user.getAddresses().get(0)); //getAddresses restituisce tutti gli address come List<Address>
            s.beginTransaction();
            addressService.unlinkAddress(user.getUsername(), address.getIdAddress());
            s.getTransaction().commit();
            User user1 = userDao.findById(user.getUserId());
            Address address1 = addressDao.findByIdAddress(address.getIdAddress());
            assertNotNull(address1);
            assertEquals(user1.getAddresses().size(), 0);
    }
}