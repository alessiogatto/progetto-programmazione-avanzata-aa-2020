package it.univpm.advprog.delivery.test;

import it.univpm.advprog.delivery.model.dao.*;
import it.univpm.advprog.delivery.model.entities.Address;
import it.univpm.advprog.delivery.model.entities.Article;
import it.univpm.advprog.delivery.model.entities.Order;
import it.univpm.advprog.delivery.model.entities.User;
import it.univpm.advprog.delivery.service.*;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.Calendar;
import java.util.TimeZone;

public class LoadDataTest {
    public static void main(String... args) {

        try (AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(DataServiceConfigTest.class)) {

            SessionFactory sf = ctx.getBean("sessionFactory", SessionFactory.class);

            AddressDao addressDao = ctx.getBean("addressDao", AddressDao.class);
            ArticleDao articleDao = ctx.getBean("articleDao", ArticleDao.class);
            CartDao cartDao = ctx.getBean("cartDao", CartDao.class);
            OrderDao orderDao = ctx.getBean("orderDao", OrderDao.class);
            UserDao userDao = ctx.getBean("userDao", UserDao.class);

            ArticleService articleService = ctx.getBean("articleService", ArticleService.class);
            OrderService orderService = ctx.getBean("orderService", OrderService.class);
            UserService userService = ctx.getBean("userService", UserService.class);
            CartService cartService = ctx.getBean("cartService", CartService.class);
            AddressService addressService = ctx.getBean("addressService", AddressService.class);

            try (Session session = sf.openSession()) {
                addressDao.setSession(session);
                userDao.setSession(session);
                orderDao.setSession(session);
                cartDao.setSession(session);
                articleDao.setSession(session);

                session.beginTransaction();

                /* ARTICOLI DEL MENU' */
                Article article1 = articleService.create("Margherita", 4.50, "classica pizza margherita", "https://th.bing.com/th/id/OIP.geOYrMAtjbvEbFW424zO_QHaHa?pid=Api&rs=1", "farina, acqua, lievito, sale ....", Article.typeArticle.PIZZE, Article.typeDieta.VEGETARIANO, 22, true);
                Article article2 = articleService.create("Fetta di torta Sacher", 5, "Ottima torta preparata da noi", "https://www.tribugolosa.com/media/sacher-6_crop.jpg/rh/torta-sacher.jpg", "farina, cioccolato, confettura di albicocca", Article.typeArticle.DOLCI, Article.typeDieta.PER_TUTTI, 4, true);
                Article article3 = articleService.create("Quattro Formaggi ", 7.50, "Ricca di formaggio come piace a noi", "https://th.bing.com/th/id/OIP.s-9r2P7vvAXR15xbIGbWIAHaHa?pid=Api&rs=1", "formaggioooooo", Article.typeArticle.PIZZE, Article.typeDieta.PER_TUTTI, 10, true);
                Article article4 = articleService.create("Capricciosa", 8, "Classica capricciosa con funghi e carciofi di giornata", "https://www.goingmywayz.com/wp-content/uploads/2017/03/pizza-Capricciosa2.jpg", "gnammmmmmm", Article.typeArticle.PIZZE, Article.typeDieta.VEGANO, 10, true);
                Article article5 = articleService.create("Antipasto della casa", 7, "bruschette in varie fantasie e formaggi locali", "https://th.bing.com/th/id/OIP.dsuO4_L-0UYE5po7lNAwmgHaHa?pid=Api&rs=1", "bruschette e formaggi", Article.typeArticle.ANTIPASTI, Article.typeDieta.PER_TUTTI, 22, true);
                Article article6 = articleService.create("Marinara", 4.50, "classica pizza marinara", "https://th.bing.com/th/id/OIP.rjnY3zp0TfcmZfL9CvyV8gHaHa?pid=Api&rs=1", "farina, acqua, lievito, sale ....", Article.typeArticle.PIZZE, Article.typeDieta.VEGETARIANO, 22, true);
                Article article7 = articleService.create("Coca Cola", 3, "coca cola in lattina, 330ml", "https://th.bing.com/th/id/OIP.ub13aVaEG0LO45tIm3pa4gHaHa?pid=Api&rs=1", "acqua e 'dddd", Article.typeArticle.BIBITE, Article.typeDieta.PER_TUTTI, 22, true);
                Article article8 = articleService.create("Tiramisù", 4, "Tiramisù casalingo", "https://th.bing.com/th/id/OIP.1TvVEuyHwZAJ1xgspWedCAHaHa?pid=Api&rs=1", "savoiardi", Article.typeArticle.DOLCI, Article.typeDieta.PER_TUTTI, 10, true);
                Article article9 = articleService.create("Diavola", 6.50, "Pizza estremamente piccante, per veri intenditori", "https://www.lapizzeria.ro/wp-content/uploads/2016/07/10-Pizza-Diavola-large.jpg", "farina, peperoncino...", Article.typeArticle.PIZZE, Article.typeDieta.VEGANO, 10, true);
                Article article10 = articleService.create("Tagliere di formaggi e salumi", 10, "Proponiamo questo ottimo antipasto, ideale per 2 persone", "https://www.siena.saporiedintorni.it/media/catalog/product/cache/1/image/1200x1200/9df78eab33525d08d6e5fb8d27136e95/t/a/tagliere-di-salumi-e-formaggi-senesi.jpg", "formaggi e salumi vari", Article.typeArticle.ANTIPASTI, Article.typeDieta.PER_TUTTI, 22, true);

                /* UTENTI ADMIN */
                User admin1 = userService.create("admin", userDao.encryptPassword("admin"), "manager", "", "admin@gma1l.com", User.typeRole.ADMIN, "+0039 123");
                User admin2= userService.create("cassa", userDao.encryptPassword("cassa"), "cassa", "", "cassa@gma1l.com", User.typeRole.ADMIN, "+0039 456");
                User admin3 = userService.create("cucina", userDao.encryptPassword("cucina"), "cucina", "", "cucina@gma1l.com", User.typeRole.ADMIN, "+0039 789");

                /* UTENTI USER (utente registrato) */
                User user1 = userService.create("user", userDao.encryptPassword("user"), "franco", "bruni", "franco@mail.com", User.typeRole.UTENTE, "+0039 111");
                User user2 = userService.create("mirko33", userDao.encryptPassword("passUser2"), "mirko", "rossi", "mirko@gma1l.com", User.typeRole.UTENTE, "+0039 222");
                User user3 = userService.create("alberto1", userDao.encryptPassword("passUser3"), "alberto", "pace", "alberto@gma1l.com", User.typeRole.UTENTE, "+0039 333");
                User user4 = userService.create("sandrino", userDao.encryptPassword("passUser4"), "alessandro", "di marco", "ale@gma1l.com", User.typeRole.UTENTE, "+0039 444");

                Address address1 = addressService.create("via monte rosa", "2c", 1000, "Ancona", Address.typeAddress.CASA);

                /* GLI UTENTI INSERISCONO UN INDIRIZZO DI RECAPITO AL LORO PROFILO */
                user1.addAddress(address1);
                user2.addAddress(addressService.create("via roma", "12", 1000, "Ancona", Address.typeAddress.LAVORO));
                user3.addAddress(addressService.create("via salle", "33", 1000, "Ancona", Address.typeAddress.PREFERITO));
                user4.addAddress(addressService.create("via carlo primo", "623", 1000, "Ancona", Address.typeAddress.LAVORO));
                /* un utente puo' avere piu' di un indirizzo, quello scelto per il recapito andra' posizionato per primo nella lista */
                user1.addAddress(addressService.create("via del baio", "68", 1000, "Ancona", Address.typeAddress.PREFERITO));
//                user1.removeAddress(address1);

                /* GLI UITENTI CREANO DEGLI ORDINI */
                Calendar calendar1 = Calendar.getInstance(TimeZone.getTimeZone("UTC")); //istanza della data e ora corrente
                Calendar calendar2 = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
                calendar1.add(Calendar.HOUR_OF_DAY, 1); // all'ora corrente aggiungo 1 ora (orario di ritiro/consegna)

                Order order1 = orderService.create(Order.typeOrder.TAKEAWAY, user1, Order.payMethodOrder.CARTA_DI_CREDITO, calendar1, "il filetto al sangue", address1.getIdAddress());
                /* AGGIUNTA ARTICOLI AL CARRELLO */
                orderService.addToCart(order1, article1,1);
                orderService.addToCart(order1, article2,1);
                orderService.addToCart(order1, article3,5);
                orderService.addToCart(order1, article4,1);
                orderService.addToCart(order1, article5,1);

                orderService.linkAddress(order1, address1);

                Order order2 = orderService.create(Order.typeOrder.TAKEAWAY, user2, Order.payMethodOrder.PAYPAL, calendar1, "le bruschette senza funghi", address1.getIdAddress());
                /* AGGIUNTA ARTICOLI AL CARRELLO */
                orderService.addToCart(order2, article3,5);
                orderService.addToCart(order2, article5,5);

                calendar2.add(Calendar.HOUR_OF_DAY, 2); // all'ora corrente aggiungo 2 ore (orario di ritiro/consegna)

                Order order3 = orderService.create(Order.typeOrder.DELIVERY, user3, Order.payMethodOrder.PAYPAL, calendar2, "la pizza con abbondante mozzarella se possibile", address1.getIdAddress() );
                /* AGGIUNTA ARTICOLI AL CARRELLO */
                orderService.addToCart(order3, article1,4);
                orderService.addToCart(order3, article5,2);

                Order order4 = orderService.create(Order.typeOrder.DELIVERY, user4, Order.payMethodOrder.CARTA_DI_CREDITO, calendar2, "arrosticini 20 fegato e 30 carne", address1.getIdAddress() );
                /* AGGIUNTA ARTICOLI AL CARRELLO */
                orderService.addToCart(order4, article3,5);
                orderService.addToCart(order4, article4,3);

                session.getTransaction().commit();

            } catch (Exception e) {
                System.out.println(e);
            }
        } catch (Exception e) {
            System.out.println(e);
        }

    }
}