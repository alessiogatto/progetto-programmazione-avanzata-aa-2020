package it.univpm.advprog.delivery.test;

import it.univpm.advprog.delivery.app.DataServiceConfig;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import java.util.Properties;

@Configuration
@ComponentScan(basePackages = {"it.univpm.advprog.delivery.model", "it.univpm.advprog.delivery.service"})
@EnableTransactionManagement


public class DataServiceConfigTest extends DataServiceConfig {
    @Bean
    @Override
    protected Properties hibernateProperties() {
        Properties hibernateProp = super.hibernateProperties();
        hibernateProp.put("javax.persistence.schema-generation.database.action", "drop-and-create");
//        hibernateProp.put("javax.persistence.schema-generation.database.action", "create");
        return hibernateProp;
    }

}