package it.univpm.advprog.delivery.controller;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import it.univpm.advprog.delivery.model.entities.Article;
import it.univpm.advprog.delivery.model.entities.Order;
import it.univpm.advprog.delivery.service.ArticleService;
import it.univpm.advprog.delivery.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.util.List;

@RequestMapping("/admin")
@Controller
public class AdminController {
    @Autowired
    private OrderService orderService;
    @Autowired
    private ArticleService articleService;

    @RequestMapping(value = "/listorders", method = RequestMethod.GET)
    public ModelAndView listorders(HttpServletRequest request, HttpServletResponse response) {
        ModelAndView mav = new ModelAndView("admin/listorders");
        List<Order> orders = orderService.findAll();
        mav.addObject("orders", new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create().toJson(orders));
        return mav;
    }

    @PostMapping("/listordersStateModified")
    public String changeStatus(Model model, @ModelAttribute("changeTo") String newState, @ModelAttribute("id") Long orderId) {

        Order order = orderService.findById(orderId);

        if (newState.equals("CONCLUDED")) {
            order.setStateOrder(Order.stateOrder.CONCLUDED);
        }
        else if(newState.equals("CONFIRMED")){
            order.setStateOrder(Order.stateOrder.CONFIRMED);
        }
        else {
            order.setStateOrder(Order.stateOrder.CANCELLED);
        }

        orderService.update(order);
        return "redirect:listorders";
    }

    @GetMapping("/cart/{orderId}")
    public ModelAndView cartDetails(HttpServletRequest request, HttpServletResponse response, @PathVariable("orderId") Long orderId) {
        ModelAndView mav = new ModelAndView("admin/cart");
        Order order = orderService.findById(orderId);
        mav.addObject("cartDetails", orderService.jsonStringify(order));
        return mav;
    }

    @RequestMapping(value = "/menu", method = RequestMethod.GET)
    public ModelAndView menu(HttpServletRequest request, HttpServletResponse response) {
        ModelAndView mav = new ModelAndView("admin/menu");
        List<Article> articles = articleService.findAll();
        mav.addObject("menu", new Gson().toJson(articles));
        return mav;
    }

    @PostMapping(value = "/menu/update")
    public String updateMenu(Model model, @ModelAttribute("newArticle") Article newArticle) {
        if (newArticle.getImageArticleURL().equals("")) {
            Article article = articleService.findById(newArticle.getIdArticle());
            String oldImage = article.getImageArticleURL();
            newArticle.setImageArticleURL(oldImage);
        } else {
            newArticle.setImageArticleURL("/delivery/resources/images/menu/" + newArticle.getImageArticleURL());
        }
        articleService.update(newArticle);
        return "redirect:/admin/menu";
    }

    @PostMapping(value = "/menu/remove")
    public ModelAndView removeArticle(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("id") int articleId) throws IOException {

        ModelAndView mav = new ModelAndView("redirect:/admin/menu/");

        Article article = articleService.findById(articleId);

        try {
            articleService.delete(article);
            mav.addObject("messageSuccess", "success");
        } catch (DataIntegrityViolationException e) {
            response.setStatus(500);
            response.getWriter().write("Errore! l'articolo non può essere eliminato");
            response.getWriter().flush();
            response.getWriter().close();
        }

        return mav;
    }

    @PostMapping(value = "/menu/add")
    public String addArticle(@ModelAttribute("newArticle") Article newArticle) {
        String nameImage = newArticle.getImageArticleURL(); //quando arriva dalla post è solo il nome del file
        articleService.create(
                newArticle.getNameArticle(),
                newArticle.getPriceArticle().doubleValue(),
                newArticle.getDescriptionArticle(),
                "/delivery/resources/images/menu/" + nameImage,
                newArticle.getIngredientsArticle(),
                newArticle.getTypeArticle(),
                newArticle.getDieta(),
                newArticle.getIva(),
                newArticle.isAvailableArticle()
        );

        return "redirect:/admin/menu";
    }

    @PostMapping(value = "/menu/addImage")
    @ResponseBody
    public void addAddress(HttpServletRequest request, HttpServletResponse response, @RequestBody MultipartFile image, HttpSession session) throws IOException {

        ServletContext context = session.getServletContext();
        String webPath = context.getRealPath("/resources/images/menu");
        String fileName = image.getOriginalFilename();
        try {
            File newImage = new File(webPath + File.separator + fileName);
            if (!newImage.exists())
                image.transferTo(new File(webPath + File.separator + fileName));
            else {
                response.setStatus(500);
                response.getWriter().write("Immagine già esistente!");
                response.getWriter().flush();
                response.getWriter().close();
            }
        } catch (IOException e) {
            response.getWriter().write("Immagine non caricata!");
            response.getWriter().flush();
            response.getWriter().close();
        }
    }

}
