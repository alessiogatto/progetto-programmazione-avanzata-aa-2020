package it.univpm.advprog.delivery.controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import it.univpm.advprog.delivery.model.entities.Address;
import it.univpm.advprog.delivery.model.entities.Article;
import it.univpm.advprog.delivery.model.entities.Order;
import it.univpm.advprog.delivery.model.entities.User;
import it.univpm.advprog.delivery.service.AddressService;
import it.univpm.advprog.delivery.service.ArticleService;
import it.univpm.advprog.delivery.service.OrderService;
import it.univpm.advprog.delivery.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Calendar;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;


@Controller
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;
    @Autowired
    private AddressService addressService;
    @Autowired
    private OrderService orderService;
    @Autowired
    private ArticleService articleService;

    @RequestMapping(value = "/menu", method = RequestMethod.GET)
    public ModelAndView menu(HttpServletRequest request, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.findUserByUsername(auth.getName());
        ModelAndView mav = new ModelAndView("user/menu");
        List<Article> articles = articleService.findAll();
        mav.addObject("menu", new Gson().toJson(articles));
        mav.addObject("user", new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create().toJson(user));
        return mav;
    }

    @PostMapping(value = "/menu/cart/checkout")
    @ResponseBody
    public String checkout(HttpServletRequest request, HttpServletResponse response, @RequestBody JsonObject jsonOrder) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.findUserByUsername(auth.getName());
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        calendar.add(Calendar.HOUR_OF_DAY, jsonOrder.get("timeDelta").getAsInt());
        String typeOrder = jsonOrder.get("type").getAsString();
        String payMethodOrder = jsonOrder.get("payMethod").getAsString();
        Order order;
        if (typeOrder.equals("TAKEAWAY")) {
            order = orderService.create(typeOrder, user, payMethodOrder, calendar, jsonOrder.get("variation").getAsString());
        } else
            order = orderService.create(typeOrder, user, payMethodOrder, calendar, jsonOrder.get("variation").getAsString(), jsonOrder.get("idAddress").getAsInt());
        JsonArray jsonCart = (JsonArray) jsonOrder.get("cart");
        jsonCart.forEach((item) -> {
            int idArticle = item.getAsJsonObject().get("item").getAsJsonObject().get("idArticle").getAsInt();
            int qnt = item.getAsJsonObject().get("qnt").getAsInt();
            orderService.addToCart(order, articleService.findById(idArticle), qnt);
        });
        orderService.update(order);
        return "/delivery/user/listorders";
    }

    @GetMapping("/profile")
    public String profile(Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.findUserByUsername(auth.getName());
        model.addAttribute("user", new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create().toJson(user));
        return "user/profile";
    }

    @PostMapping("/profile/update")
    public String updateUser(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("user") User user) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User userAuth = userService.findUserByUsername(auth.getName());
        userService.setUserToUpdate(userAuth, user);
        return "redirect:/user/profile";
    }

    @GetMapping(value = "/address")
    public String showAddress(Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.findUserByUsername(auth.getName());
        model.addAttribute("user", new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create().toJson(user));
        return "user/address";
    }

    @PostMapping(value = "/address/update")
    public String updateAddress(Model model, @ModelAttribute("newAddress") Address newAddress) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        addressService.unlinkAddress(auth.getName(), newAddress.getIdAddress());
        addressService.delete(newAddress.getIdAddress());
        Address address = addressService.update(newAddress);
        addressService.linkAddress(auth.getName(), address.getIdAddress());
        return "redirect:/user/address";
    }

    @PostMapping(value = "/address/remove")
    public String removeAddress(Model model, @ModelAttribute("id") int idaddress, HttpServletRequest req, HttpServletResponse response) throws IOException {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        User user = userService.findUserByUsername(auth.getName());
        Set<Order> orders = user.getOrdersUser();

        boolean flag = false;

        for (Order order : orders) {
            if (order.getIdAddress() == idaddress) {
                flag = true;
                break;
            }
        }
        if (!flag) {
            addressService.unlinkAddress(auth.getName(), idaddress);
            addressService.delete(idaddress);
        } else {
            response.setStatus(500);
            response.getWriter().write("Errore! L'indirizzo non può essere eliminato!");
            response.getWriter().flush();
            response.getWriter().close();
        }
        return "user/address";
    }

    @PostMapping(value = "/address/add")
    public String addAddress(Model model, @ModelAttribute("newAddress") Address newAddress) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        int capAddress = newAddress.getCapAddress();
        String streetAddress = newAddress.getStreetAddress();
        String numberAddress = newAddress.getNumberAddress();
        String cityAddress = newAddress.getCityAddress();
        Address.typeAddress typeAddress = newAddress.getTypeAddress();
        Address address = addressService.create(streetAddress, numberAddress, capAddress, cityAddress, typeAddress);
        addressService.linkAddress(auth.getName(), address.getIdAddress());
        return "redirect:/user/address";
    }


    @RequestMapping(value = "/listorders", method = RequestMethod.GET)
    public ModelAndView listorders(HttpServletRequest request, HttpServletResponse response) {
        ModelAndView mav = new ModelAndView("user/listorders");
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.findUserByUsername(auth.getName());
        Set<Order> orders = user.getOrdersUser();
        mav.addObject("orders", new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create().toJson(orders));
        return mav;
    }

    @GetMapping("/cart/{orderId}")
    public ModelAndView cartDetails(HttpServletRequest request, HttpServletResponse response, @PathVariable("orderId") Long orderId) {
        ModelAndView mav = new ModelAndView("user/cart");
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.findUserByUsername(auth.getName());
        Set<Order> orders = user.getOrdersUser();
        Boolean exist = false;

        for (Order order : orders) {
            if (order.getNumberOrder().equals(orderId)) {
                exist = true;
            }
        }

        if (exist) {
            Order order = orderService.findById(orderId);
            mav.addObject("cartDetails", orderService.jsonStringify(order));
            return mav;
        } else {
            return new ModelAndView("redirect:https://bit.ly/37Waqet"); // :)
        }
    }

}

