package it.univpm.advprog.delivery.controller;

import com.google.gson.Gson;
import it.univpm.advprog.delivery.model.entities.Article;
import it.univpm.advprog.delivery.model.entities.User;
import it.univpm.advprog.delivery.service.ArticleService;
import it.univpm.advprog.delivery.service.UserService;
import it.univpm.advprog.delivery.utils.Login;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.regex.Pattern;

@Controller
public class HomeController {

    @Autowired
    private UserService userService;
    @Autowired
    private ArticleService articleService;

    public static final Pattern validEmail = Pattern.compile("(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])\n", Pattern.CASE_INSENSITIVE);

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView home(HttpServletRequest request, HttpServletResponse response) {
        ModelAndView mav = new ModelAndView("home");
        return mav;
    }

    @RequestMapping(value = "/menu", method = RequestMethod.GET)
    public ModelAndView menu(HttpServletRequest request, HttpServletResponse response) {
        ModelAndView mav = new ModelAndView("menu");
        List<Article> articles = articleService.findAll();
        mav.addObject("menu", new Gson().toJson(articles));
        return mav;
    }

    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public ModelAndView showRegister(HttpServletRequest request, HttpServletResponse response) {
        ModelAndView mav = new ModelAndView("register");
        mav.addObject("user", new User());
        return mav;
    }

    @RequestMapping(value = "/registerProcess", method = RequestMethod.POST)
    public ModelAndView addUser(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("user") User user) {
        ModelAndView mav = new ModelAndView("register");
        try {
            userService.create(
                    user.getUsername(),
                    userService.encryptPassword(user.getPassword()),
                    user.getFirstNameUser(),
                    user.getLastNameUser(),
                    user.getMailUser(),
                    User.typeRole.UTENTE,
                    user.getPhoneNumberUser()
            );
            mav.addObject("messageSuccess", "Registrato con successo! Fai il login.");
        } catch (ConstraintViolationException e) {
            mav.addObject("messageError", "Username o email non disponibile!");
        }
        return mav;
    }

    @RequestMapping(value = "/loginProcess", method = RequestMethod.POST)
    public ModelAndView loginProcess(HttpServletRequest request, HttpServletResponse response,
                                     @ModelAttribute("login") Login login) {
        ModelAndView mav = null;
        User user = userService.validateUser(login);
        if (user != null) {
            mav = new ModelAndView("redirect:/");
        } else {
            mav = new ModelAndView("login");
        }
        return mav;
    }

    @RequestMapping("/accessDenied")
    public String accessDenied() {
        return "accessDenied";
    }

}

