package it.univpm.advprog.delivery.utils;

import it.univpm.advprog.delivery.model.entities.User;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UserMapper implements RowMapper<User> {
    public User mapRow(ResultSet rs, int arg1) throws SQLException {
        User user = new User();
        user.setUsername(rs.getString(User.USERNAME));
        user.setPassword(rs.getString(User.PASSWORD));
        user.setFirstNameUser(rs.getString(User.FIRST_NAME));
        user.setLastNameUser(rs.getString(User.LAST_NAME));
        user.setMailUser(rs.getString(User.MAIL));
        user.setPhoneNumberUser(rs.getString(User.PHONE));
        return user;
    }
}