package it.univpm.advprog.delivery.service;

import com.google.gson.Gson;
import it.univpm.advprog.delivery.model.dao.CartDao;
import it.univpm.advprog.delivery.model.entities.Article;
import it.univpm.advprog.delivery.model.entities.Cart;
import it.univpm.advprog.delivery.model.entities.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
@Service("cartService")
public class CartServiceDefault implements CartService {

    @Autowired
    private CartDao cartItemRepository;

    @Override
    public Cart findById(Long cartItemId) {
        return this.cartItemRepository.findById(cartItemId);
    }

    @Override
    public Cart create(int quantityArticle, Article articleCart, Order order) {
        return this.cartItemRepository.create(order, articleCart, quantityArticle);
    }

    @Override
    public void cartRemove(Cart cart) {
        this.cartItemRepository.delete(cart);
    }

    @Override
    public List<Cart> findAll() {
        return this.cartItemRepository.findAll();
    }

    @Override
    public String jsonStringify(Cart cart) {
        return new Gson().toJson(cart);
    }

    @Override
    public List<Cart> findByOrderID(Order order) {
        return this.cartItemRepository.findByOrderID(order);
    }


}
