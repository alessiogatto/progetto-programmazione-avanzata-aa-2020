package it.univpm.advprog.delivery.service;

import it.univpm.advprog.delivery.model.entities.Article;
import it.univpm.advprog.delivery.model.entities.Cart;
import it.univpm.advprog.delivery.model.entities.Order;

import java.util.List;

public interface CartService {

    Cart findById(Long cartItemId);

    Cart create(int quantityArticle, Article articleCart, Order order);

    void cartRemove(Cart cart);

    List<Cart> findAll();

    String jsonStringify(Cart cart);

    List<Cart> findByOrderID(Order order);
}
