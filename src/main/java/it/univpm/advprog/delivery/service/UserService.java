package it.univpm.advprog.delivery.service;

import it.univpm.advprog.delivery.model.entities.User;
import it.univpm.advprog.delivery.utils.Login;

import java.util.List;

public interface UserService {
    User create(String username, String password, String firstNameUser, String lastNameUser, String mailUser, User.typeRole role, String phoneNumberUser);

    User update(User user);

    User findById(Long clientId);

    void delete(User user);

    List<User> findAll();

    String encryptPassword(String password);

    User validateUser(Login login);

    User findUserByUsername(String username);

    String jsonStringify(User user);

    User setUserToUpdate(User auth, User user);
}
