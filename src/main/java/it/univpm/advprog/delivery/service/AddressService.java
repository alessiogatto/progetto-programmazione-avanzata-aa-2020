package it.univpm.advprog.delivery.service;

import it.univpm.advprog.delivery.model.entities.Address;

import java.util.List;

public interface AddressService {

    Address create(String streetAddress, String numberAddress, int capAddress, String cityAddress, Address.typeAddress typeAddress);

    Address update(Address address);

    Address findByIdAddress(int idAddress);

    void delete(int idAddress);

    List<Address> findAll();

    String jsonStringify(Address address);

    void unlinkAddress(String username, int addressId);

    void linkAddress(String username, int addressId);

    void unlinkAddresses(String username, List<Address> addresses);

    void linkAddresses(String username, List<Address> addresses);
}
