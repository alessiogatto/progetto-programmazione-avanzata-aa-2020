package it.univpm.advprog.delivery.service;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import it.univpm.advprog.delivery.model.dao.UserDao;
import it.univpm.advprog.delivery.model.entities.User;
import it.univpm.advprog.delivery.utils.Login;
import it.univpm.advprog.delivery.utils.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.core.userdetails.User.UserBuilder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
@Service("userService")
public class UserServiceDefault implements UserService, UserDetailsService {

    @Autowired
    private UserDao userRepository;

    @Autowired
    private JdbcTemplate jdbcConnection;

    @Autowired
    private AddressService addressService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        User user = userRepository.findUserByUsername(username);
        UserBuilder userBuilder;

        if (user != null) {

            //mapping nel nostro user in un user di spring
            userBuilder = org.springframework.security.core.userdetails.User.withUsername(username);
            userBuilder.disabled(!user.isEnabled()); //se user enabled -> nel builder user non disabilitato, se user notEnabled -> nel builder user disabilitato
            userBuilder.password(user.getPassword());

            if (user.getRole().equals(User.typeRole.ADMIN)) {
                userBuilder.roles(User.typeRole.ADMIN.name());
            } else {
                userBuilder.roles(User.typeRole.UTENTE.name());
            }
        } else {
            throw new UsernameNotFoundException("User not found!");
        }
        return userBuilder.build();
    }

    @Transactional
    @Override
    public User create(String username, String password, String firstNameUser, String lastNameUser, String mailUser, User.typeRole role, String phoneNumberUser) {
        return this.userRepository.create(username, password, firstNameUser, lastNameUser, mailUser, role, phoneNumberUser);
    }

    @Transactional
    @Override
    public User update(User user) {
        return this.userRepository.update(user);
    }

    @Transactional
    @Override
    public User setUserToUpdate(User auth, User user){
        user.setUserId(auth.getUserId());
        user.setRole(auth.getRole());
        user.setEnabled(true);
        user.setPassword(this.encryptPassword(user.getPassword()));
        this.update(user);
        addressService.linkAddresses(auth.getUsername(), auth.getAddresses());
        return user;

    }

    @Transactional
    @Override
    public User findById(Long clientId) {
        return this.userRepository.findById(clientId);
    }

    @Override
    public void delete(User user) {
        this.userRepository.delete(user);
    }

    @Override
    public List<User> findAll() {
        return this.userRepository.findAll();
    }

    @Override
    public String encryptPassword(String password) {
        return this.userRepository.encryptPassword(password);
    }

    @Override
    public User validateUser(Login login) {
        String sql = "select * from users where USERNAME=?";
        String username = login.getUsername();
        String password = login.getPassword();

        try {
            List<User> users = jdbcConnection.query(sql, new UserMapper(), username);
            BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
            return encoder.matches(password, users.get(0).getPassword()) ? users.get(0) : null;
        } catch (IndexOutOfBoundsException e) {
            return null;
        }
    }

    @Override
    public User findUserByUsername(String username) {
        return this.userRepository.findUserByUsername(username);
    }

    @Override
    public String jsonStringify(User user) {
        JsonObject js = JsonParser.parseString(new Gson().toJson(user)).getAsJsonObject();
        JsonArray jsonArray = new JsonArray();
        jsonArray.addAll(
                JsonParser.parseString(
                        user.getAddresses().toString()
                ).getAsJsonArray()
        );
        js.add("addresses", jsonArray);
        return js.getAsString();
    }
}