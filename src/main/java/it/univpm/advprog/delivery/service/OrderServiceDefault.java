package it.univpm.advprog.delivery.service;

import com.google.gson.GsonBuilder;
import com.google.inject.internal.util.Lists;
import it.univpm.advprog.delivery.model.dao.CartDao;
import it.univpm.advprog.delivery.model.dao.OrderDao;
import it.univpm.advprog.delivery.model.entities.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

@Transactional
@Service("orderService")
public class OrderServiceDefault implements OrderService {

    @Autowired
    private OrderDao orderRepository;
    @Autowired
    private CartDao cartItemRepository;

    @Autowired
    public void setOrderRepository(OrderDao orderRepository) {
        this.orderRepository = orderRepository;
    }

    @Override
    public Order create(Order.typeOrder typeOrder, User userOrder, Order.payMethodOrder payMethodOrder, Calendar pickUpTimeOrder, String variationRequest, int idAddress) {

        Calendar creationDate = Calendar.getInstance(TimeZone.getTimeZone("UTC"));

        return this.orderRepository.create(typeOrder, userOrder, payMethodOrder, creationDate, pickUpTimeOrder, variationRequest, idAddress);
    }
    @Transactional
    @Override
    public Order create(String typeOrderString, User userOrder, String payMethodOrderString, Calendar pickUpTimeOrder, String variationRequest, int idAddress) {

        Calendar creationDate = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        Order.typeOrder typeOrder = Order.typeOrder.TAKEAWAY;
        if (typeOrderString.equals("DELIVERY")) {
            typeOrder = Order.typeOrder.DELIVERY;
        }

        Order.payMethodOrder payMethodOrder = Order.payMethodOrder.CONTANTI;

        if (payMethodOrderString.equals("CARTA_DI_CREDITO")) {
            payMethodOrder = Order.payMethodOrder.CARTA_DI_CREDITO;
        } else if (payMethodOrderString.equals("PAYPAL")) {
            payMethodOrder = Order.payMethodOrder.PAYPAL;
        }

        return this.orderRepository.create(typeOrder, userOrder, payMethodOrder, creationDate, pickUpTimeOrder, variationRequest, idAddress);
    }
    @Transactional
    @Override
    public Order create(String typeOrderString, User userOrder, String payMethodOrderString, Calendar pickUpTimeOrder, String variationRequest) {
        Calendar creationDate = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        Order.typeOrder typeOrder = Order.typeOrder.TAKEAWAY;
        if (typeOrderString.equals("DELIVERY")) {
            typeOrder = Order.typeOrder.DELIVERY;
        }

        Order.payMethodOrder payMethodOrder = Order.payMethodOrder.CONTANTI;

        if (payMethodOrderString.equals("CARTA_DI_CREDITO")) {
            payMethodOrder = Order.payMethodOrder.CARTA_DI_CREDITO;
        } else if (payMethodOrderString.equals("PAYPAL")) {
            payMethodOrder = Order.payMethodOrder.PAYPAL;
        }

        return this.orderRepository.create(typeOrder, userOrder, payMethodOrder, creationDate, pickUpTimeOrder, variationRequest);
    }


    @Transactional
    @Override
    public Order update(Order order) {
        return this.orderRepository.update(order);
    }

    @Transactional
    @Override
    public Order findById(Long idOrder) {
        return this.orderRepository.findById(idOrder);
    }

    @Transactional
    @Override
    public void delete(Order order) {
        this.orderRepository.delete(order);
    }

    @Transactional
    @Override
    public List<Order> findAll() {
        return this.orderRepository.findAll();
    }

    @Override
    public void linkAddress(Order order, Address address) {
        order.setIdAddress(address.getIdAddress());
        orderRepository.update(order);
    }

    @Transactional
    @Override
    public void addToCart(Order order, Article article, int quantity) {
        Cart cart = cartItemRepository.create(order, article, quantity);
        order.addCartToOrder(cart);
    }

    @Transactional
    @Override
    public void removeFromCart(Order order, Cart cart) {
        order.removeCartToOrder(cart);
        cartItemRepository.delete(cart);
    }

    @Override
    public String jsonStringify(Order order) {
        List<Cart> list = cartItemRepository.findByOrderID(order);
        String json = (new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create().toJson(list));
        return json;
    }

    @Override
    public List<String> listStringify(List<Order> orders) {
        List<String> jsonCarts = Lists.newArrayList();
        orders.forEach(order -> {
            jsonCarts.add((jsonStringify(order)));
        });
        return jsonCarts;
    }
}
