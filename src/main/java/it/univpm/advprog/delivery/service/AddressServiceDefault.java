package it.univpm.advprog.delivery.service;

import com.google.gson.Gson;
import it.univpm.advprog.delivery.model.dao.AddressDao;
import it.univpm.advprog.delivery.model.dao.UserDao;
import it.univpm.advprog.delivery.model.entities.Address;
import it.univpm.advprog.delivery.model.entities.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
@Service("addressService")
public class AddressServiceDefault implements AddressService {

    @Autowired
    private AddressDao addressRepository;

    @Autowired
    private UserDao userRepository;

    @Override
    public Address create(String streetAddress, String numberAddress, int capAddress, String cityAddress, Address.typeAddress typeAddress) {
        return this.addressRepository.create(streetAddress, numberAddress, capAddress, cityAddress, typeAddress);
    }

    @Override
    public Address update(Address address) {
        return this.addressRepository.update(address);
    }

    @Override
    public Address findByIdAddress(int idAddress) {
        return this.addressRepository.findByIdAddress(idAddress);
    }

    @Override
    @Transactional
    public void delete(int id) {
        Address address = addressRepository.findByIdAddress(id);
        addressRepository.delete(address);
    }

    @Override
    @Transactional
    public void unlinkAddress(String username, int addressId) {
        User user = userRepository.findUserByUsername(username);
        Address address = addressRepository.findByIdAddress(addressId);
        user.getAddresses().remove(address);
        userRepository.update(user);
    }

    @Override
    @Transactional
    public void linkAddress(String username, int addressId) {
        User user = userRepository.findUserByUsername(username);
        Address address = addressRepository.findByIdAddress(addressId);
        user.getAddresses().add(address);
        userRepository.update(user);
    }

    @Override
    @Transactional
    public void unlinkAddresses(String username, List<Address> addresses) {
        addresses.forEach(item -> unlinkAddress(username, item.getIdAddress()));
    }

    @Override
    @Transactional
    public void linkAddresses(String username, List<Address> addresses) {
        addresses.forEach(item -> linkAddress(username, item.getIdAddress()));
    }

    @Transactional
    @Override
    public List<Address> findAll() {
        return this.addressRepository.findAll();
    }

    @Override
    public String jsonStringify(Address address) {
        return new Gson().toJson(address);
    }

}
