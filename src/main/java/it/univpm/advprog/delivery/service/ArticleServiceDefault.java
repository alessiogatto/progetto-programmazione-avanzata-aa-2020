package it.univpm.advprog.delivery.service;

import com.google.gson.Gson;
import it.univpm.advprog.delivery.model.dao.ArticleDao;
import it.univpm.advprog.delivery.model.entities.Article;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
@Service("articleService")
public class ArticleServiceDefault implements ArticleService {

    @Autowired
    private ArticleDao articleRepository;

    // GETTER E SETTER NON MI SERVONO SE FACCIO AUTOWIRED SULL'ATTRIBUTO

    @Transactional
    @Override
    public void toggleAvailable(Article article) {
        article.setAvailableArticle(!article.isAvailableArticle());
    }

    @Transactional
    @Override
    public Article create(String nameArticle, double priceArticle, String descriptionArticle, String imageArticleURL, String ingredientsArticle, Article.typeArticle typeArticle, Article.typeDieta dieta, int iva, boolean availableArticle) {
        return this.articleRepository.create(nameArticle, priceArticle, descriptionArticle, imageArticleURL, ingredientsArticle, typeArticle, dieta, iva, availableArticle);
    }

    @Transactional
    @Override
    public Article update(Article article) {
        return this.articleRepository.update(article);
    }

    @Transactional(readOnly = true)
    @Override
    public Article findById(int idArticle) {
        return this.articleRepository.findById(idArticle);
    }

    @Transactional
    @Override
    public void delete(Article article) {
        this.articleRepository.delete(article);
    }

    @Transactional
    @Override
    public List<Article> findAll() {
        return this.articleRepository.findAll();
    }

    @Override
    public String jsonStringify(Article article) {
        return new Gson().toJson(article);
    }
}