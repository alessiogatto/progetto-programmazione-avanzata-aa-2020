package it.univpm.advprog.delivery.service;

import it.univpm.advprog.delivery.model.entities.Article;

import java.util.List;

public interface ArticleService {

    Article create(String nameArticle, double priceArticle, String descriptionArticle, String imageArticleURL, String ingredientsArticle, Article.typeArticle typeArticle, Article.typeDieta dieta, int iva, boolean availableArticle);

    Article update(Article article);

    Article findById(int idArticle);

    void delete(Article article);

    List<Article> findAll();

    String jsonStringify(Article article);

    void toggleAvailable(Article article);
}
