package it.univpm.advprog.delivery.service;

import it.univpm.advprog.delivery.model.entities.*;

import java.util.Calendar;
import java.util.List;

public interface OrderService {
    Order create(
            Order.typeOrder typeOrder,
            User userOrder,
            Order.payMethodOrder payMethodOrder,
            Calendar pickUpTimeOrder,
            String variationRequest,
            int idAddress
    );

    Order create(
            String typeOrder,
            User userOrder,
            String payMethodOrder,
            Calendar pickUpTimeOrder,
            String variationRequest,
            int idAddress
    );

    Order create(
            String typeOrder,
            User userOrder,
            String payMethodOrder,
            Calendar pickUpTimeOrder,
            String variationRequest
    );

    Order update(Order order);

    Order findById(Long idOrder);

    void delete(Order order);

    List<Order> findAll();

    void linkAddress(Order order, Address address);

    void addToCart(Order order, Article article, int quantity);

    void removeFromCart(Order order, Cart cart);

    String jsonStringify(Order order);

    List<String> listStringify(List<Order> orders);

}
