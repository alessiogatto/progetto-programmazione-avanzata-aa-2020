package it.univpm.advprog.delivery.model.entities;

import com.google.gson.annotations.Expose;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name="cart")

public class Cart implements Serializable {

    public static final String QUANTITY = "QUANTITY";
    public static final String ARTICLE_ID = "ARTICLE_ID";
    public static final String ORDER_ID = "ORDER_ID";
    @Expose
    private int quantityArticle;
    @Expose
    private Article articleCartItem;
    @Expose
    private int idCartItem;
    @Expose
    private Order order;


    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getIdCartItem() {
        return idCartItem;
    }

    @Column(name = QUANTITY, nullable = false)
    public int getQuantityArticle() {
        return quantityArticle;
    }

    @ManyToOne
    @JoinColumn(name = ARTICLE_ID)
    public Article getArticleCartItem() {
        return articleCartItem;
    }

    @ManyToOne
    @JoinColumn(name = ORDER_ID)
    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public void setQuantityArticle(int quantityArticle) {
        this.quantityArticle = quantityArticle;
    }

    public void setArticleCartItem(Article articleCartItem) {
        this.articleCartItem = articleCartItem;
    }

    public void setIdCartItem(int idCartItem) {
        this.idCartItem = idCartItem;
    }

}
