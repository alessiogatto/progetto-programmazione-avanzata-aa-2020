package it.univpm.advprog.delivery.model.entities;


import com.google.gson.annotations.Expose;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Calendar;


@Entity
@Table(name = "orders")
public class Order implements Serializable {

    public static final String STATE_ORDER = "STATE_ORDER";
    public static final String CREATION_DATE = "CREATION_DATE";
    public static final String TYPE_ORDER = "TYPE_ORDER";
    public static final String PAY_METHOD = "PAY_METHOD";
    public static final String PICKUP_TIME = "PICKUP_TIME";
    public static final String TOTAL_AMOUNT = "TOTAL_AMOUNT";
    public static final String VARIATION_REQUEST = "VARIATION_REQUEST";
    public static final String ID_USER = "ID_USER";
    public static final String ID_ADDRESS = "ID_ADDRESS";

    public enum payMethodOrder {CONTANTI, PAYPAL, CARTA_DI_CREDITO}

    public enum typeOrder {DELIVERY, TAKEAWAY}

    public enum stateOrder {NEW, CONFIRMED, CONCLUDED, CANCELLED}

    @Expose
    private Long numberOrder;
    @Expose
    private stateOrder stateOrder;
    @Expose
    private Calendar creationDateOrder;
    @Expose
    private typeOrder typeOrder;
    @Expose
    private payMethodOrder payMethodOrder;
    @Expose
    private Calendar pickUpTimeOrder; // orario di ritiro/consegna
    @Expose
    private double totalAmountOrder;
    @Expose
    private User userOrder;
    @Expose
    private String variationRequest;  // richieste particolari del cliente Es. cottura al sangue
    @Expose
    private int idAddress;

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long getNumberOrder() {
        return numberOrder;
    }

    @Column(name = STATE_ORDER)
    public Order.stateOrder getStateOrder() {
        return stateOrder;
    }

    @Column(name = CREATION_DATE)
    public Calendar getCreationDateOrder() {
        return creationDateOrder;
    }

    @Column(name = TYPE_ORDER)
    public typeOrder getTypeOrder() {
        return typeOrder;
    }

    @Column(name = PICKUP_TIME)
    public Calendar getPickUpTimeOrder() {
        return pickUpTimeOrder;
    }

    @Column(name = TOTAL_AMOUNT)
    public double getTotalAmountOrder() {
        return totalAmountOrder;
    }

    @Column(name = VARIATION_REQUEST)
    public String getVariationRequest() {
        return variationRequest;
    }

    @Column(name = PAY_METHOD)
    public payMethodOrder getPayMethodOrder() {
        return payMethodOrder;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = ID_USER)
    public User getUserOrder() {
        return userOrder;
    }

    @Column(name = ID_ADDRESS)
    public int getIdAddress() {
        return idAddress;
    }

    public void setIdAddress(int idAddress) {
        this.idAddress = idAddress;
    }

    public void setStateOrder(Order.stateOrder stateOrder) {
        this.stateOrder = stateOrder;
    }

    public void setUserOrder(User userOrder) {
        this.userOrder = userOrder;
    }

    public void setNumberOrder(Long numberOrder) {
        this.numberOrder = numberOrder;
    }

    public void setCreationDateOrder(Calendar creationDateOrder) {
        this.creationDateOrder = creationDateOrder;
    }

    public void setPayMethodOrder(payMethodOrder payMethodOrder) {
        this.payMethodOrder = payMethodOrder;
    }

    public void setPickUpTimeOrder(Calendar pickUpTimeOrder) {
        this.pickUpTimeOrder = pickUpTimeOrder;
    }

    public void setTotalAmountOrder(double totalAmountOrder) {
        this.totalAmountOrder = totalAmountOrder;
    }

    public void setTypeOrder(typeOrder typeOrder) {
        this.typeOrder = typeOrder;
    }

    public void setVariationRequest(String description) {
        this.variationRequest = description;
    }

    public void addCartToOrder(Cart cart) {

        Article article = cart.getArticleCartItem();
        double toAdd = article.getPriceArticle().doubleValue() * cart.getQuantityArticle();
        this.totalAmountOrder += toAdd;
    }

    public void removeCartToOrder(Cart cart) {

        Article article = cart.getArticleCartItem();
        double toSubtract = article.getPriceArticle().doubleValue() * cart.getQuantityArticle();
        this.totalAmountOrder -= toSubtract;
    }

}
