package it.univpm.advprog.delivery.model.dao;


import it.univpm.advprog.delivery.model.entities.Address;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import java.util.List;


@Repository("addressDao")
//@Transactional
public class AddressDaoDefault extends DefaultDao implements AddressDao {

    @Override
    public Address create(String streetAddress, String numberAddress, int capAddress, String cityAddress, Address.typeAddress typeAddress) {
        String numberStr = String.valueOf(capAddress);
        if (numberStr.length() >= 5) {
            capAddress = Integer.parseInt(numberStr.substring(0, 5));
        }

        Address address = new Address();
        address.setStreetAddress(StringUtils.capitalize(streetAddress));
        address.setNumberAddress(numberAddress);
        address.setCapAddress(capAddress);
        address.setCityAddress(StringUtils.capitalize(cityAddress));
        address.setTypeAddress(typeAddress);
        this.getSession().save(address);
        return address;
    }

    @Override
    public Address update(Address address) {
        return (Address) this.getSession().merge(address);
    }

    @Override
    public Address findByIdAddress(int idAddress) {
        return getSession().find(Address.class, idAddress);
    }

    @Override
    public void delete(Address address) {
        this.getSession().delete(address);
    }

    @Override
    public List<Address> findAll() {
        return getSession().createQuery("from Address a", Address.class).getResultList();
    }

}
