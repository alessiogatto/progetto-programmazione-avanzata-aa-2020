package it.univpm.advprog.delivery.model.dao;

import it.univpm.advprog.delivery.model.entities.Article;
import it.univpm.advprog.delivery.model.entities.Cart;
import it.univpm.advprog.delivery.model.entities.Order;
import org.hibernate.Session;

import java.util.List;

public interface CartDao {

    Session getSession();

    public void setSession(Session session);

    Cart findById(Long cartItemId);

    Cart create(Order order, Article articleCartItem, int quantityArticle);

    void delete(Cart cart);

    List<Cart> findAll();

    List<Cart> findByOrderID(Order order);
}
