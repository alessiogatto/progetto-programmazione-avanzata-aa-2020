package it.univpm.advprog.delivery.model.dao;


import it.univpm.advprog.delivery.model.entities.Order;
import it.univpm.advprog.delivery.model.entities.User;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import java.util.Calendar;
import java.util.List;


@Repository("orderDao")
//@Transactional
public class OrderDaoDefault extends DefaultDao implements OrderDao {
    @Override
    public Order create(Order.typeOrder typeOrder,
                        User userOrder,
                        Order.payMethodOrder payMethodOrder,
                        Calendar creationDateOrder,
                        Calendar pickUpTimeOrder,
                        String variationRequest, int idAddress) {

        Order order = new Order();
        order.setStateOrder(Order.stateOrder.NEW); // valore di default per nuovi ordini
        order.setTypeOrder(typeOrder);
        order.setUserOrder(userOrder);
        order.setPayMethodOrder(payMethodOrder);
        order.setCreationDateOrder(creationDateOrder);
        order.setPickUpTimeOrder(pickUpTimeOrder);
        order.setVariationRequest(variationRequest);
        order.setIdAddress(idAddress);
        this.getSession().save(order);
        return order;
    }

    @Override
    public Order create(Order.typeOrder typeOrder, User userOrder, Order.payMethodOrder payMethodOrder, Calendar creationDateOrder, Calendar pickUpTimeOrder, String variationRequest) {
        Order order = new Order();
        order.setStateOrder(Order.stateOrder.NEW); // valore di default per nuovi ordini
        order.setTypeOrder(typeOrder);
        order.setUserOrder(userOrder);
        order.setPayMethodOrder(payMethodOrder);
        order.setCreationDateOrder(creationDateOrder);
        order.setPickUpTimeOrder(pickUpTimeOrder);
        order.setVariationRequest(StringUtils.capitalize(variationRequest));
        this.getSession().save(order);
        return order;
    }

    @Override
    public Order update(Order order) {
        return (Order) this.getSession().merge(order);
    }

    @Override
    public Order findById(Long idOrder) {
        return this.getSession().find(Order.class, idOrder);
    }


    @Override
    public void delete(Order order) {
        this.getSession().delete(order);
    }

    @Override
    public List<Order> findAll() {
        return this.getSession().createQuery("SELECT o FROM Order o", Order.class).getResultList();
    }

}
