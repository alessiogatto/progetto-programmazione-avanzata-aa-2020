package it.univpm.advprog.delivery.model.entities;


import com.google.gson.annotations.Expose;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "addresses")
public class Address implements Serializable {

    public static final String STREET = "STREET";
    public static final String NUMBER = "NUMBER";
    public static final String CAP = "CAP";
    public static final String CITY = "CITY";
    public static final String TYPE_ADDRESS = "TYPE_ADDRESS";

    public enum typeAddress {LAVORO, CASA, PREFERITO}

    @Expose
    private int idAddress;
    @Expose
    private String streetAddress;
    @Expose
    private String numberAddress;
    @Expose
    private int capAddress;
    @Expose
    private String cityAddress;
    @Expose
    private typeAddress typeAddress;

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getIdAddress() {
        return this.idAddress;
    }

    @Column(name = CAP, nullable = false)
    public int getCapAddress() {
        return this.capAddress;
    }

    @Column(name = STREET, nullable = false)
    public String getStreetAddress() {
        return this.streetAddress;
    }

    @Column(name = CITY, nullable = false)
    public String getCityAddress() {
        return this.cityAddress;
    }

    @Column(name = NUMBER, nullable = false)
    public String getNumberAddress() {
        return this.numberAddress;
    }

    @Column(name = TYPE_ADDRESS)
    public Address.typeAddress getTypeAddress() {
        return typeAddress;
    }

    public void setTypeAddress(Address.typeAddress typeAddress) {
        this.typeAddress = typeAddress;
    }

    public void setNumberAddress(String numberAddress) {
        this.numberAddress = numberAddress;
    }

    public void setCapAddress(int capAddress) {
        this.capAddress = capAddress;
    }

    public void setCityAddress(String cityAddress) {
        this.cityAddress = cityAddress;
    }

    public void setIdAddress(int idAddress) {
        this.idAddress = idAddress;
    }

    public void setStreetAddress(String streetAddress) {
        this.streetAddress = streetAddress;
    }

    @Override
    public String toString() {
        return (this.streetAddress + " n." + this.numberAddress + " cap." + this.capAddress + " " + this.cityAddress);
    }

}