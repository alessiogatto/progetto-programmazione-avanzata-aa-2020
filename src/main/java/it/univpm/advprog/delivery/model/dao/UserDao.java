package it.univpm.advprog.delivery.model.dao;

import it.univpm.advprog.delivery.model.entities.User;
import org.hibernate.Session;

import java.util.List;

public interface UserDao {

    Session getSession();

    void setSession(Session session);

    User create(String username, String password, String firstNameUser, String lastNameUser, String mailUser, User.typeRole role, String phoneNumberUser);

    User update(User user);

    User findById(Long clientId);

    User findUserByUsername(String username);

    void delete(User user);

    List<User> findAll();

    String encryptPassword(String password);
}
