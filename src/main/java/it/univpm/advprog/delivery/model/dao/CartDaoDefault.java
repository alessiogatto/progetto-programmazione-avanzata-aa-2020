package it.univpm.advprog.delivery.model.dao;

import it.univpm.advprog.delivery.model.entities.Article;
import it.univpm.advprog.delivery.model.entities.Cart;
import it.univpm.advprog.delivery.model.entities.Order;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("cartDao")
public class CartDaoDefault extends DefaultDao implements CartDao {

    @Override
    public Cart findById(Long cartItemId) {
        return getSession().find(Cart.class, cartItemId);
    }

    @Override
    public Cart create(Order order, Article articleCartItem, int quantityArticle) {
        Cart cart = new Cart();
        cart.setQuantityArticle(quantityArticle);
        cart.setOrder(order);
        cart.setArticleCartItem(articleCartItem);
        this.getSession().save(cart);
        return cart;
    }

    @Override
    public void delete(Cart cart) {
        this.getSession().delete(cart);
    }

    @Override
    public List<Cart> findAll() {
        return getSession().createQuery("SELECT a FROM Cart a", Cart.class).getResultList();
    }

    @Override
    public List<Cart> findByOrderID(Order order) {
        return this.getSession().createQuery("SELECT c FROM Cart c where c.order=:order", Cart.class).setParameter("order", order).getResultList();
    }


}