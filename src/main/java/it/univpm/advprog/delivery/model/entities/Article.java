package it.univpm.advprog.delivery.model.entities;

import com.google.gson.annotations.Expose;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Table(name = "articles")

public class Article implements Serializable {

    public static final String NAME = "NAME";
    public static final String PRICE = "PRICE";
    public static final String DESCRIPTION = "DESCRIPTION";
    public static final String IMAGE_URL = "IMAGE_URL";
    public static final String INGREDIENTS = "INGREDIENTS";
    public static final String TYPE_ARTICLE = "TYPE_ARTICLE";
    public static final String DIET = "DIET";
    public static final String IVA = "IVA";

    public enum typeDieta {VEGANO, VEGETARIANO, PER_TUTTI}

    public enum typeArticle {PIZZE, BIBITE, DOLCI, ANTIPASTI}

    @Expose
    private int idArticle;
    @Expose
    private String nameArticle;
    @Expose
    private BigDecimal priceArticle;
    @Expose
    private String descriptionArticle;
    @Expose
    private String imageArticleURL;
    @Expose
    private String ingredientsArticle;
    @Expose
    private typeArticle typeArticle;
    @Expose
    private typeDieta dieta;
    @Expose
    private int iva;
    @Expose
    private boolean availableArticle;

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getIdArticle() {
        return this.idArticle;
    }

    @Column(name = "AVAILABLE", nullable = false)
    public boolean isAvailableArticle() {
        return availableArticle;
    }

    public void setAvailableArticle(boolean availableArticle) {
        this.availableArticle = availableArticle;
    }

    @Column(name = NAME, nullable = false)
    public String getNameArticle() {
        return nameArticle;
    }

    @Column(name = IVA, precision = 3, scale = 2)
    public int getIva() {
        return iva;
    }

    @Column(name = PRICE, nullable = false, precision = 10, scale = 2)
    public BigDecimal getPriceArticle() {
        return priceArticle;
    }

    @Column(name = DESCRIPTION)
    public String getDescriptionArticle() {
        return descriptionArticle;
    }

    @Column(name = IMAGE_URL)
    public String getImageArticleURL() {
        return imageArticleURL;
    }

    @Column(name = INGREDIENTS)
    public String getIngredientsArticle() {
        return ingredientsArticle;
    }

    @Column(name = TYPE_ARTICLE, nullable = false)
    public typeArticle getTypeArticle() {
        return typeArticle;
    }

    @Column(name = DIET, nullable = false)
    public typeDieta getDieta() {
        return dieta;
    }

    public void setDieta(typeDieta dieta) {
        this.dieta = dieta;
    }

    public void setIdArticle(int idArticle) {
        this.idArticle = idArticle;
    }

    public void setNameArticle(String nameArticle) {
        this.nameArticle = nameArticle;
    }

    public void setPriceArticle(BigDecimal priceArticle) {
        this.priceArticle = priceArticle;
    }

    public void setIva(int iva) {
        this.iva = iva;
    }

    public void setDescriptionArticle(String descriptionArticle) {
        this.descriptionArticle = descriptionArticle;
    }

    public void setImageArticleURL(String imageArticleURL) {
        this.imageArticleURL = imageArticleURL;
    }

    public void setIngredientsArticle(String ingredientsArticle) {
        this.ingredientsArticle = ingredientsArticle;
    }

    public void setTypeArticle(typeArticle typeArticle) {
        this.typeArticle = typeArticle;
    }

}
