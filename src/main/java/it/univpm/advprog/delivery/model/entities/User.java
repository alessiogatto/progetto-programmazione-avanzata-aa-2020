package it.univpm.advprog.delivery.model.entities;


import com.google.gson.annotations.Expose;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@DynamicUpdate
@Table(name = "users")

public class User {

    public static final String PHONE = "PHONE";
    public static final String USERNAME = "USERNAME";
    public static final String PASSWORD = "PASSWORD";
    public static final String FIRST_NAME = "FIRST_NAME";
    public static final String LAST_NAME = "LAST_NAME";
    public static final String MAIL = "MAIL";
    public static final String ROLE = "ROLE";

    public enum typeRole {UTENTE, ADMIN}

    @Expose
    private Long userId;
    @Expose
    private String username;
    private String password;
    @Expose
    private String firstNameUser;
    @Expose
    private String lastNameUser;
    @Expose
    private String mailUser;
    @Expose
    private String phoneNumberUser;
    @Expose
    private typeRole role;
    @Column(name = "ENABLED", nullable = false)
    @Expose
    private boolean enabled;
    @Expose
    private List<Address> addresses = new ArrayList<Address>();
    private Set<Order> ordersUser = new HashSet<Order>();

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long getUserId() {
        return this.userId;
    }

    @Column(name = PHONE, nullable = false)
    public String getPhoneNumberUser() {
        return this.phoneNumberUser;
    }

    @Column(name = USERNAME, nullable = false, unique = true, updatable = false)
    public String getUsername() {
        return this.username;
    }

    @Column(name = PASSWORD, nullable = false)
    public String getPassword() {
        return this.password;
    }

    @Column(name = FIRST_NAME, nullable = false)
    public String getFirstNameUser() {
        return this.firstNameUser;
    }

    @Column(name = LAST_NAME, nullable = false)
    public String getLastNameUser() {
        return this.lastNameUser;
    }

    @Column(name = MAIL, nullable = false, unique = true)
    public String getMailUser() {
        return this.mailUser;
    }

    @ManyToMany(fetch = FetchType.EAGER) /*cascade = CascadeType.ALL*/
    @JoinTable(name = "address_user",
            joinColumns = {@JoinColumn(name = "USER_ID")},
            inverseJoinColumns = {@JoinColumn(name = "ADDRESS_ID")})
    @Column
    public List<Address> getAddresses() {
        return this.addresses;
    }

    @OneToMany(mappedBy = "userOrder", fetch = FetchType.EAGER)
    public Set<Order> getOrdersUser() {
        return ordersUser;
    }

    @Column(name = ROLE, nullable = false, updatable = false)
    public typeRole getRole() {
        return role;
    }

    public boolean isEnabled() {
        return this.enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public void setPhoneNumberUser(String phoneNumberUser) {
        this.phoneNumberUser = phoneNumberUser;
    }

    public void setAddresses(List<Address> addresses) {
        this.addresses = addresses;
    }

    public void setRole(typeRole role) {
        this.role = role;
    }

    public void setOrdersUser(Set<Order> ordersUser) {
        this.ordersUser = ordersUser;
    }

    public void setMailUser(String mailUser) {
        this.mailUser = mailUser;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public void setLastNameUser(String lastNameUser) {
        this.lastNameUser = lastNameUser;
    }

    public void setFirstNameUser(String firstNameUser) {
        this.firstNameUser = firstNameUser;
    }

    public void addAddress(Address address) {
        this.addresses.add(address);
    }

    public void removeAddress(Address address) {
        this.addresses.remove(address);
    }

    @Override
    public String toString() {
        return firstNameUser + " " + lastNameUser;
    }
}
