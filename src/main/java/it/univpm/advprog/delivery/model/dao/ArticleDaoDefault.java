package it.univpm.advprog.delivery.model.dao;

import it.univpm.advprog.delivery.model.entities.Article;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.util.List;

@Repository("articleDao")
public class ArticleDaoDefault extends DefaultDao implements ArticleDao{
    @Override
    public Article create(String nameArticle, double priceArticle, String descriptionArticle, String imageArticleURL, String ingredientsArticle, Article.typeArticle typeArticle, Article.typeDieta dieta, int iva, boolean available) {
        Article article = new Article();
        article.setNameArticle(StringUtils.capitalize(nameArticle));
        article.setDescriptionArticle(StringUtils.capitalize(descriptionArticle));
        article.setPriceArticle(new BigDecimal(priceArticle));
        article.setImageArticleURL(imageArticleURL);
        article.setTypeArticle(typeArticle);
        article.setIngredientsArticle(StringUtils.capitalize(ingredientsArticle));
        article.setDieta(dieta);
        article.setIva(iva);
        article.setAvailableArticle(available);
        this.getSession().save(article);
        return article;
    }


    @Override
    public Article update(Article article) {
        return (Article) this.getSession().merge(article);
    }

    @Override
    public Article findById(int idArticle) {
        return getSession().find(Article.class, idArticle);
    }

    @Override
    public void delete(Article article) {
        this.getSession().delete(article);
    }

    @Override
    public List<Article> findAll() {
        return getSession().createQuery("from Article a", Article.class).getResultList();
    }
}
