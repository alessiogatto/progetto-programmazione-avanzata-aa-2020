package it.univpm.advprog.delivery.model.dao;


import it.univpm.advprog.delivery.model.entities.Order;
import it.univpm.advprog.delivery.model.entities.User;
import org.hibernate.Session;

import java.util.Calendar;
import java.util.List;


public interface OrderDao {

    Session getSession();

    public void setSession(Session session);

    Order create(Order.typeOrder typeOrder, User userOrder, Order.payMethodOrder payMethodOrder, Calendar creationDateOrder, Calendar pickUpTimeOrder, String variationRequest, int idAddress);

    Order create(Order.typeOrder typeOrder, User userOrder, Order.payMethodOrder payMethodOrder, Calendar creationDateOrder, Calendar pickUpTimeOrder, String variationRequest);

    Order update(Order order);

    Order findById(Long idOrder);

    void delete(Order order);

    List<Order> findAll();


}
