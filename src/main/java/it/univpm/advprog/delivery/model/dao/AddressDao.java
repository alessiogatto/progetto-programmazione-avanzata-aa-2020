package it.univpm.advprog.delivery.model.dao;

import it.univpm.advprog.delivery.model.entities.Address;
import org.hibernate.Session;

import java.util.List;

public interface AddressDao {

    Session getSession();

    public void setSession(Session session);

    Address create(String streetAddress, String numberAddress, int capAddress, String cityAddress, Address.typeAddress typeAddress);

    Address update(Address address);

    Address findByIdAddress(int idAddress);

    List<Address> findAll();

    void delete(Address address);
}
