package it.univpm.advprog.delivery.model.dao;

import it.univpm.advprog.delivery.model.entities.Article;
import org.hibernate.Session;

import java.util.List;


public interface ArticleDao {

    Session getSession();

    public void setSession(Session session);

    Article create(String nameArticle, double priceArticle, String descriptionArticle, String imageArticleURL, String ingredientsArticle, Article.typeArticle typeArticle, Article.typeDieta dieta, int iva, boolean available);

    Article update(Article article);

    Article findById(int idArticle);

    void delete(Article article);

    List<Article> findAll();
}
