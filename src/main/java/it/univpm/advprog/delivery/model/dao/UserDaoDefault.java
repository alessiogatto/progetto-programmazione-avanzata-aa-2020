package it.univpm.advprog.delivery.model.dao;


import it.univpm.advprog.delivery.model.entities.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.List;

@Repository("userDao")
//@Transactional
public class UserDaoDefault extends DefaultDao implements UserDao {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public User create(String username, String password, String firstNameUser, String lastNameUser, String mailUser, User.typeRole role, String phoneNumberUser) {
        User user = new User();
        user.setFirstNameUser(StringUtils.capitalize(firstNameUser));
        user.setPassword(password);
        user.setLastNameUser(StringUtils.capitalize(lastNameUser));
        user.setMailUser(mailUser);
        user.setUsername(username);
        user.setRole(role);
        user.setPhoneNumberUser(phoneNumberUser);
        user.setEnabled(true);
        this.getSession().save(user);
        return user;
    }

    @Transactional
    @Override
    public User update(User user) {
        return (User) this.getSession().merge(user);
    }

    @Override
    public User findById(Long clientId) {
        return this.getSession().find(User.class, clientId);
    }

    @Override
    public User findUserByUsername(String username) {
        return this.getSession().createQuery("SELECT u FROM User u WHERE u.username = :username", User.class).setParameter("username", username).getSingleResult();
    }

    @Override
    public void delete(User user) {
        this.getSession().delete(user);
    }

    @Override
    public List<User> findAll() {
        return getSession().createQuery("SELECT u FROM User u", User.class).getResultList();
    }

    @Override
    public String encryptPassword(String password) {
        return this.passwordEncoder.encode(password);
    }

}

