package it.univpm.advprog.delivery.security;

import it.univpm.advprog.delivery.service.UserServiceDefault;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                .antMatchers("/").permitAll()
                .antMatchers("/register").permitAll()   //per registrarsi
                .antMatchers("/home").permitAll()   //home page con menu e filtri
                .antMatchers("/user/**").hasAnyRole("UTENTE")
                .antMatchers("/admin/**").hasAnyRole("ADMIN")
                .and()
                .formLogin().loginPage("/register").loginProcessingUrl("/loginProcess")
                .defaultSuccessUrl("/").and().exceptionHandling().accessDeniedPage("/accessDenied")
                .and().logout()
                .invalidateHttpSession(true)
                .and().csrf().disable();
    }

    @Bean
    public UserDetailsService userDetailsService() { // A UserDetailsService loads User (entity) via the user’s username.
        return new UserServiceDefault(); // UserServiceDefault deve implementare UserDetailsService e il suo metodo loadUserByUsername
    }

    @Autowired
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService()).passwordEncoder(this.passwordEncoder);
    }

}