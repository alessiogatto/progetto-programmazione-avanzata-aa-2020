<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page session="false" %>


<!-- Prendiamo la variabile menu dall'home controller. Questa variabile la usiamo nel main.js -->

<sec:authorize access="hasRole('ADMIN')" var="isAdmin" />
<sec:authorize access="hasRole('UTENTE')" var="isUser" />
<sec:authorize access="isAuthenticated()" var="isAuth"/>

<style>
    .entry {
        margin: 5rem auto;
    }

    .title {
        margin: 1rem auto;
    }
</style>

<div id="banner" class="banner full-screen-mode parallax">
    <div class="container pr">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="banner-static">
                <div class="banner-text">
                    <div class="banner-cell">
                        <div class="entry">
                            <h1 class="title">Cena con noi <span class="typer" id="some-id" data-delay="200"
                                                                 data-delim=":"
                                                                 data-words="a casa:dagli amici:in ufficio"
                                                                 data-colors="red"></span><span class="cursor"
                                                                                                data-cursorDisplay="_"
                                                                                                data-owner="some-id"></span>
                            </h1>
                            <h2 class="title">Se si fa in quattro per renderti felice, è una pizza</h2>
                            <c:if test="${isUser}">
                            <div class="book-btn entry">
                                <a href="${pageContext.request.contextPath}/user/menu"
                                   class="table-btn hvr-underline-from-center">Ordina Ora</a>
                            </div>
                            </c:if>

                            <c:if test="${!isAuth}">
                                <div class="book-btn entry">
                                    <a href="${pageContext.request.contextPath}/menu"
                                       class="table-btn hvr-underline-from-center">Ordina Ora</a>
                                </div>
                            </c:if>

                            <c:if test="${isAdmin}">
                                <div class="book-btn entry">
                                    <a href="${pageContext.request.contextPath}/admin/listorders"
                                       class="table-btn hvr-underline-from-center">Gestisci Ordini</a>
                                </div>
                            </c:if>


                        </div>
                        <!-- end banner-cell -->
                    </div>
                    <!-- end banner-text -->
                </div>
                <!-- end banner-static -->
            </div>
            <!-- end col -->
        </div>
        <!-- end container -->
    </div>
</div>