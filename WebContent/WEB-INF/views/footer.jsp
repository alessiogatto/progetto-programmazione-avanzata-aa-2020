<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"
           prefix="c" %>

<div id="footer" class="footer-main">
  <div class="footer-box pad-top-70">
    <div class="container">
      <div class="row">
        <div class="footer-in-main">
          <div class="footer-logo">
            <div class="text-center">
<%--              <img src="${pageContext.request.contextPath}/resources/images/logo.png" alt="" />--%>
              <img src="${pageContext.request.contextPath}/resources/images/logo1.png" alt=""/>
            </div>
          </div>
          <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="footer-box-a">
              <h3>Chi siamo</h3>
              <p>Tre ragazzi simpatici e molto social:</p>
              <ul class="socials-box footer-socials pull-left">
                <li>
                  <a href= "https://www.youtube.com/watch?v=dQw4w9WgXcQ">
                    <div class="social-circle-border"><i class="fa  fa-facebook"></i></div>
                  </a>
                </li>
                <li>
                  <a href= "https://www.youtube.com/watch?v=dQw4w9WgXcQ">
                    <div class="social-circle-border"><i class="fa fa-twitter"></i></div>
                  </a>
                </li>
                <li>
                  <a href= "https://www.youtube.com/watch?v=dQw4w9WgXcQ">
                    <div class="social-circle-border"><i class="fa fa-youtube"></i></div>
                  </a>
                </li>
                <li>
                  <a href= "https://www.youtube.com/watch?v=dQw4w9WgXcQ">
                    <div class="social-circle-border"><i class="fa fa-pinterest"></i></div>
                  </a>
                </li>
                <li>
                  <a href= "https://www.youtube.com/watch?v=dQw4w9WgXcQ">
                    <div class="social-circle-border"><i class="fa fa-linkedin"></i></div>
                  </a>
                </li>
              </ul>

            </div>
            <!-- end footer-box-a -->
          </div>
          <!-- end col -->
          <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="footer-box-b">
              <h3>Menu nuovo!</h3>
              <p>
                Un fantastico menu con tante novità ma comunque identico a prima!
              </p>
            </div>
            <!-- end footer-box-b -->
          </div>
          <!-- end col -->
          <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="footer-box-c">
              <h3>Che aspetti, non contattarci!</h3>
              <p>
                <i class="fa fa-map-signs" aria-hidden="true"></i>
                <span>via doro, Furci 123, Isola che non c'è</span>
              </p>
              <p>
                <i class="fa fa-mobile" aria-hidden="true"></i>
                <span>
									+91 80005 89080
								</span>
              </p>
              <p>
                <i class="fa fa-envelope" aria-hidden="true"></i>
                <span><a href= "https://www.youtube.com/watch?v=dQw4w9WgXcQ">viva@spring.com</a></span>
              </p>
            </div>
            <!-- end footer-box-c -->
          </div>
          <!-- end col -->
          <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="footer-box-d">
              <h3>Orario di apertura</h3>

              <ul>
                <li>
                  <p>Mertedì - Venerdì </p>
                  <span> 2:00 AM - 4:00 AM</span>
                </li>
                <li>
                  <p>Venerdì - Domenica </p>
                  <span>  7:00 AM - 7:20 PM</span>
                </li>
              </ul>
            </div>
            <!-- end footer-box-d -->
          </div>
          <!-- end col -->
        </div>
        <!-- end footer-in-main -->
      </div>
      <!-- end row -->
    </div>
    <!-- end container -->
    <div id="copyright" class="copyright-main">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h6 class="copy-title"> Copyright &copy; 2021 offerto da <a href= "https://www.youtube.com/watch?v=dQw4w9WgXcQ"> Rick Astley </a> </h6>
          </div>
        </div>
        <!-- end row -->
      </div>
      <!-- end container -->
    </div>
    <!-- end copyright-main -->
  </div>
  <!-- end footer-box -->
</div>
<!-- end footer-main -->


