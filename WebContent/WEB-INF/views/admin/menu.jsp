<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page session="false" %>

<script> var menu = ${menu};  </script>

<style>
    table.table td a.edit {
        color: #FFC107;
    }

    table.table td a.delete {
        color: #F44336;
    }
</style>

<div class="listOrdersContainer myBackground container-fluid">
    <div class="table-wrapper">
        <div class="table-title">
            <div class="row">
                <div class="col-sm-4">
                    <h2 style="color: white"><b>Menu</b></h2>
                </div>
                <div class="col-sm-8">
                    <a id="new-add-btn" onclick="addNewArticle()" class="btn btn-success" data-toggle="modal"><i
                            class="material-icons">&#xE147;</i>
                        <span>Aggiungi articolo</span></a>
                    <a href="${pageContext.request.contextPath}/admin/menu" class="btn btn-primary">
                        <i class="material-icons">&#xE863;</i>
                        <span>Ricarica</span>
                    </a>
                </div>
            </div>
        </div>
        <div class="table-filter">
            <div class="row">
                <div class="col-sm">
                    <div class="filter-group">
                        <label>Type</label>
                        <select id="typeFilter" class="form-control">
                            <option value="ALL">All</option>
                            <option value="ANTIPASTI">Antipasti</option>
                            <option value="PIZZE">Pizze</option>
                            <option value="DOLCI">Dolci</option>
                            <option value="BIBITE">Bibite</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <form:form id="addForm" modelAttribute="newArticle" action="/delivery/admin/menu/add" method="POST">
            <table class="table table-striped table-hover">
                <thead>
                <tr>
                    <th>Articolo</th>
                    <th>Prezzo</th>
                    <th>Descrizione</th>
                    <th>Immagine</th>
                    <th>Ingredienti</th>
                    <th>Tipo</th>
                    <th>Dieta</th>
                    <th>Iva</th>
                    <th>Disponibile</th>
                    <td>Azioni</td>
                </tr>
                </thead>
                <tbody id="menu">
                    <%--list orders popolata in main.js--%>
                </tbody>
            </table>
        </form:form>
    </div>
</div>

