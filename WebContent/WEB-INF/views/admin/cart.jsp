<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page session="false" %>

<script> var cartDetails = ${cartDetails};  </script>

<div id="adminCartList" class="listOrdersContainer myBackground container-fluid">
    <div class="table-wrapper">
        <div class="table-title">
            <div class="row">
                <div class="col-sm-4">
                    <h2 style="color: white">Cart <b>Details</b></h2>
                </div>
                <div class="col-sm-8">
                    <a href="${pageContext.request.contextPath}/admin/cart/${orderId}" class="btn btn-primary">
                        <i class="material-icons">&#xE863;</i>
                        <span>Refresh List</span>
                    </a>
                </div>
            </div>
        </div>
        <table class="table table-striped table-hover">
            <thead>
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>Quantity</th>
                <th>Diet</th>
            </tr>
            </thead>
            <tbody id="cartBody">
            <%--                cart popolato in main.js--%>
            </tbody>
        </table>
    </div>
</div>

