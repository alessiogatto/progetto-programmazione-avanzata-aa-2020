<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"
           prefix="c" %>
<%@ taglib prefix="fn"
           uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="sec"
           uri="http://www.springframework.org/security/tags" %>
<%@ page session="false" %>

<script> var orders = ${orders};  </script>

<div id="adminListOrder" class="listOrdersContainer myBackground container-fluid">
    <div class="table-wrapper">
        <div class="table-title">
            <div class="row">
                <div class="col-sm-4">
                    <h2 style="color: white">Dettagli <b>Ordini</b></h2>
                </div>
                <div class="col-sm-8">
                    <a href="${pageContext.request.contextPath}/admin/listorders" class="btn btn-primary">
                        <i class="material-icons">&#xE863;</i>
                        <span>Aggiorna</span>
                    </a>
                </div>
            </div>
        </div>
        <div class="table-filter">
            <div class="row">
                <div class="col-sm">
                    <div class="filter-group">
                        <label>Status</label>
                        <select id="statusFilter" class="form-control">
                            <option value="ALL">Tutti</option>
                            <option value="NEW">Nuovi</option>
                            <option value="CONFIRMED">Confermati</option>
                            <option value="CONCLUDED">Conclusi</option>
                            <option value="CANCELLED">Cancellato</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>

        <table class="table table-striped table-hover">
            <thead>
            <tr>
                <th>Cliente</th>
                <th>Indirizzo</th>
                <th>Data di creazione</th>
                <th>Orario di ritiro</th>
                <th>Stato</th>
                <th>Totale</th>
                <th>Richieste del cliente</th>
                <th>Metodo di pagamento</th>
                <th>Tipo di ordine</th>
                <th>Dettagli</th>
                <th>Conferma</th>
                <th>Concludi</th>
                <th>Cancella</th>
            </tr>
            </thead>
            <tbody id="listOrders">
            <%--list orders popolata in main.js--%>
            </tbody>
        </table>


    </div>
</div>


<%--<div id="banner" class="banner full-screen-mode parallax">--%>
<%--    <div class="container pr">--%>
<%--        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">--%>
<%--            <div class="banner-static">--%>
<%--                <div class="banner-text">--%>
<%--                    <div class="banner-cell">--%>
<%--                    </div>--%>
<%--                    <!-- end banner-cell -->--%>
<%--                </div>--%>
<%--                <!-- end banner-text -->--%>
<%--            </div>--%>
<%--            <!-- end banner-static -->--%>
<%--        </div>--%>
<%--        <!-- end col -->--%>
<%--    </div>--%>
<%--    <!-- end container -->--%>
<%--</div>--%>