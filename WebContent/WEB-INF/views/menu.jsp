<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page session="false" %>

<script> var menu = ${menu};  </script>

<div id="home" class="conteiner-fluid myBackground">
    <main role="main">
        <div id="menu" class="menu-main pad-top-100 pad-bottom-100 myBackground">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.1s">
                            <h2 class="block-title text-center">
                                Il nostro Menu
                            </h2>
                            <p class="title-caption text-center">Da sempre attenti alla qualità, ti proponiamo la nostra
                                varietà di prodotti sempre freschi e genuini. </p>
                        </div>
                        <div class="tab-menu">
                            <div class="slider slider-nav">
                                <div class="tab-title-menu">
                                    <h2>Antipasti</h2>
                                    <p><i class="flaticon-canape"></i></p>
                                </div>
                                <div class="tab-title-menu">
                                    <h2>Pizze</h2>
                                    <p><i class="flaticon-dinner"></i></p>
                                </div>
                                <div class="tab-title-menu">
                                    <h2>Dolci</h2>
                                    <p><i class="flaticon-desert"></i></p>
                                </div>
                                <div class="tab-title-menu">
                                    <h2>Bibite</h2>
                                    <p><i class="flaticon-coffee"></i></p>
                                </div>
                            </div>
                            <div class="slider slider-single">
                                <div class="antipasti">
                                    <!--  QUI CI SONO GLI ANTIPASTI, ottenuti da main.js e dal json creato nel controller &#45;&#45;%>-->
                                </div>
                                <div class="pizze">
                                    <!-- QUI CI SONO LE PIZZE, ottenuti da main.js e dal json creato nel controller &#45;&#45;%>-->
                                </div>
                                <div class="dolci">
                                    <!-- ;&#45; QUI CI SONO I DOLCI, ottenuti da main.js e dal json creato nel controller &#45;&#45;%>-->
                                </div>
                                <div class="bibite">
                                    <!--; QUI CI SONO LE BIBITE, ottenuti da main.js e dal json creato nel controller &#45;&#45;%>-->
                                </div>

                            </div>
                        </div>
                        <!-- end tab-menu -->
                    </div>
                    <!-- end col -->
                </div>
                <!-- end row -->
            </div>
            <!-- end container -->
        </div>
        <!-- end menu -->
    </main>
</div>


