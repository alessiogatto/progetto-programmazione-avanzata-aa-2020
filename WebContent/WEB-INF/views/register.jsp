<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page session="false" %>

<style>

    .reg {
        width: fit-content;
        padding: 0 4rem;
        border-collapse: separate;
        border-left: 4px dotted rgba(164, 86, 95, 0.8);

    }

    .log {
        width: fit-content;
        padding: 0 4rem;
        border-collapse: collapse;
        border-right: 4px dotted rgba(164, 86, 95, 0.8);
    }

    .loginPage, #register {
        margin: 5rem auto;
    }

    .loginPage h2, #register h2 {
        margin: auto;
        padding-bottom: 0px;
        width: fit-content;
    }

    .loginPage table, #register table {
        width: fit-content;
    }

    .loginPage .alert, #register .alert {
        width: fit-content;
    }

    #loginForm, #regform_div {
        margin: auto;
    }

    #loginForm label, #loginForm input, #searchsub,
    #regform_div label, #regform_div input, #searchsub2 {
        margin: 10px auto;
    }

    #loginForm input, #searchsub,
    #regform_div input, #searchsub2 {
        padding: 5px 7px;
        color: black;
    }

</style>

<div id="banner" class="banner full-screen-mode parallax">
    <div class="container pr">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="banner-static">
                <div class="banner-text">
                    <div class="banner-cell">
                        <div class="center">
                            <table>
                                <tr>
                                    <td class="log">
                                        <div class="loginPage">
                                            <c:url value="/loginProcess" var="action_url"/>
                                            <form id="loginForm" modelAttribute="login" method="POST" action="${action_url}">
                                                <h2>Login</h2>
                                                <table class="center">
                                                    <tr>
                                                        <td><label>Username</label></td>
                                                    </tr>
                                                    <tr>
                                                        <td><input type='text'
                                                                   placeholder="Username" name="username"></td>
                                                    </tr>
                                                    <tr>
                                                        <td><label>Password</label></td>
                                                    </tr>
                                                    <tr>
                                                        <td><input type='password'
                                                                   placeholder="Password" name="password"></td>
                                                    </tr>
                                                </table>

                                                <div class="h4Margin" id="searchsub">
                                                    <input type="submit" value="Login">
                                                </div>
                                            </form>

                                            <c:if test="${param.logout != null}">
                                                <div class="container alert alert-success h4Margin">
                                                    <h2>
                                                        Grazie per aver utilizzato il servizio!
                                                    </h2>
                                                </div>
                                            </c:if>

                                            <c:if test="${param.error != null}">
                                                <div class="container alert alert-danger h4Margin">
                                                    <h2>
                                                        Login fallito!
                                                    </h2>
                                                </div>
                                            </c:if>


                                        </div>
                                    </td>
                                    <td class="reg">
                                        <div id="register" class="center">
                                            <c:url value="/registerProcess" var="action_url"/>
                                            <h2>Registrazione</h2>
                                            <form:form id="registerForm" modelAttribute="user" method="POST" action="${action_url}"
                                                       onsubmit="return continueOrNot()">
                                                <div id="regform_div">

                                                    <table class="h4Margin">
                                                        <tr>
                                                            <td><label>E-mail</label></td>
                                                        </tr>
                                                        <tr>
                                                            <td onchange="validateEmail()">
                                                                <input placeholder='E-mail' name='mailUser' id="mailUser">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <span id="messageEmail"></span>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td><label>Username</label></td>
                                                        </tr>
                                                        <tr>
                                                            <td><input placeholder='Username' name='username'></td>
                                                        </tr>
                                                    </table>
                                                    <br>
                                                    <table class="h4Margin">
                                                        <tr>
                                                            <td><label>Password</label></td>
                                                            <td><label>Ripeti Password</label></td>
                                                        </tr>
                                                        <tr>
                                                            <td onchange="validate()">
                                                                <input class="tdMargin" type='password' id='password'
                                                                       placeholder='Password' name='password'>
                                                            </td>
                                                            <td onchange="validate()">
                                                                <input class="tdMargin" type='password' id='password2'
                                                                       placeholder='Ripeti Password'>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <table class="h4Margin">
                                                        <tr>
                                                            <td>
                                                                <label id="messagePassword"></label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <table class="h4Margin">
                                                        <tr>
                                                            <td><label>Nome</label></td>
                                                            <td><label>Cognome</label></td>
                                                        </tr>
                                                        <tr>
                                                            <td><input class="tdMargin" placeholder='Nome' name='firstNameUser'
                                                                       id='firstNameUser'></td>
                                                            <td><input class="tdMargin" placeholder='Cognome' name='lastNameUser'
                                                                       id='lastNameUser'></td>
                                                        </tr>
                                                    </table>
                                                    <br>
                                                    <table class="h4Margin">
                                                        <tr>
                                                            <td><label>Numero di telefono</label></td>
                                                        </tr>
                                                        <tr>
                                                            <td><input placeholder='Numero di telefono' name='phoneNumberUser'
                                                                       id='phoneNumberUser'></td>
                                                        </tr>
                                                    </table>

                                                    <div>
                                                        <label><input type="checkbox" required="required"> Accetto i <a style="text-decoration: underline !important;color: #fff56c;" href="https://cutt.ly/8hJvOsH">termini e condizioni</a></label>
                                                    </div>

                                                    <div class="h4Margin" id='searchsub2'>
                                                        <input id='submit' type='submit' value='Registrami'>
                                                    </div>
                                                </div>
                                            </form:form>

                                            <c:if test="${messageError != null}">
                                                <div class="container alert alert-warning h4Margin">
                                                    <h2>
                                                            ${messageError}
                                                    </h2>
                                                </div>
                                            </c:if>

                                            <c:if test="${messageSuccess != null}">
                                                <div class="container alert alert-success h4Margin">
                                                    <h2>
                                                            ${messageSuccess}
                                                    </h2>
                                                </div>
                                            </c:if>

                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <!-- end banner-cell -->
                    </div>
                    <!-- end banner-text -->
                </div>
                <!-- end banner-static -->
            </div>
            <!-- end col -->
        </div>
        <!-- end container -->
    </div>
</div>


