<%@ page pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<link
        rel="stylesheet"
        type="text/css"
        href="${pageContext.request.contextPath}/resources/css/accessDenied.css"/>

    <div class="scene container-fluid center">
        <div class="overlay"></div>
        <div class="overlay"></div>
        <div class="overlay"></div>
        <div class="overlay"></div>
        <span class="bg-403">403</span>
        <div class="text">
            <span class="hero-text"></span>
            <span class="msg">Non puoi accedere a questa area!</span>
            <span class="support">
                                    <span>Qualcosa è andato storto?</span>
                                    <a href="${pageContext.request.contextPath}/contacts">Contattaci!</a>
                                </span>
        </div>
        <div class="lock"></div>
    </div>
</div>

