<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page session="false" %>

<style>
    .loginPage{
        margin: 10rem auto;
    }
    .loginPage h2{
        margin: auto;
        padding-bottom: 0px;
        width: fit-content;
    }
    .loginPage table{
        width: fit-content;
    }

    .loginPage .alert{
        width: fit-content;
    }

    #loginForm{
        margin: auto;
    }

    #loginForm label, #loginForm input #searchsub{
        margin: 10px auto;
    }

    #loginForm input, #searchsub{
        padding: 5px 7px;
        color: black;
    }

</style>

<div class="loginPage">
    <c:url value="/loginProcess" var="action_url"/>
    <form id="loginForm" modelAttribute="login" method="POST" action="${action_url}">
        <h2>LOGIN</h2>
        <table class="center">
            <tr>
                <td><label>Username</label></td>
            </tr>
            <tr>
                <td><input type='text'
                           placeholder="Username" name="username"></td>
            </tr>
            <tr>
                <td><label>Password</label></td>
            </tr>
            <tr>
                <td><input type='password'
                           placeholder="Password" name="password"></td>
            </tr>
        </table>

        <div class="h4Margin" id="searchsub">
            <input type="submit" value="Invio">
        </div>
    </form>

    <c:if test="${param.logout != null}">
        <div class="container alert alert-success h4Margin">
            <h2>
                Grazie per aver utilizzato il servizio!
            </h2>
        </div>
    </c:if>

    <c:if test="${param.error != null}">
        <div class="container alert alert-danger h4Margin">
            <h2>
                Login fallito!
            </h2>
        </div>
    </c:if>

</div>
