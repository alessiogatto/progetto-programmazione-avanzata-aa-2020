<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<sec:authorize access="hasRole('ADMIN')" var="isAdmin" />
<sec:authorize access="hasRole('UTENTE')" var="isUser" />
<sec:authorize access="isAuthenticated()" var="isAuth"/>
<div id="site-header">
    <header id="header" class="header-block-top">
        <div class="container">
            <div class="row">
                <div class="main-menu">
                    <!-- navbar -->
                    <nav class="navbar navbar-default" id="mainNav">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <div class="logo">
                                <span class="navbar-brand js-scroll-trigger logo-header">
                                    <img src="${pageContext.request.contextPath}/resources/images/logo1.png" alt="">
                                </span>
                            </div>
                        </div>
                        <div id="navbar" class="navbar-collapse collapse">
                            <ul class="nav navbar-nav navbar-right" style="margin: auto 0px">

                                <li id="nav-home" class="active"><a href="${pageContext.request.contextPath}/">Home</a></li>

                                <c:if test="${isAdmin}">
                                    <li id="nav-admin-menu"><a href="${pageContext.request.contextPath}/menu">Menu</a></li>
                                </c:if>

                                <c:if test="${isAdmin}">
                                    <li id="nav-admin-menu"><a href="${pageContext.request.contextPath}/admin/menu">Gestisci Menu</a></li>
                                </c:if>

                                <c:if test="${isAdmin}">
                                    <li id="nav-ordini"><a href="${pageContext.request.contextPath}/admin/listorders">Ordini</a></li>
                                </c:if>

                                <c:if test="${isUser}">
                                    <li id="nav-admin-menu"><a href="${pageContext.request.contextPath}/user/menu">Menu</a></li>
                                </c:if>

                                <c:if test="${isUser}">
                                    <li id="user-profile"><a href="${pageContext.request.contextPath}/user/profile">Profilo</a></li>
                                </c:if>

                                <c:if test="${isUser}">
                                    <li id="nav-ordini"><a href="${pageContext.request.contextPath}/user/listorders">I Miei Ordini</a></li>
                                </c:if>

                                <c:if test="${isUser}">
                                    <li id="user-address"><a href="${pageContext.request.contextPath}/user/address">I miei indirizzi</a></li>
                                </c:if>

                                <li id="contattaci"><a href="#footer">Contattaci</a></li>

                                <c:if test="${isAuth}">
                                    <li id="logout"><a href="${pageContext.request.contextPath}/logout">Logout</a></li>
                                </c:if>

                                <c:if test="${!isAuth}">
                                    <li id="nav-registrati"><a href="${pageContext.request.contextPath}/register">Login/Registrati</a></li>
                                </c:if>
                            </ul>
                        </div>
                        <!-- end nav-collapse -->
                    </nav>
                    <!-- end navbar -->
                </div>
            </div>
            <!-- end row -->
        </div>
        <!-- end container-fluid -->
    </header>
    <!-- end header -->
</div>
<!-- end site-header -->