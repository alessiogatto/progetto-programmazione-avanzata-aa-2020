<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page session="false" %>

<script> var user = ${user};  </script>

<style>

    #profile{
        margin: 5rem auto;
    }

    #profile h2 {
        margin: auto;
        padding-bottom: 0px;
        width: fit-content;
    }

    #profile table {
        width: fit-content;
    }

    #profile .alert {
        width: fit-content;
    }

    #profile label, #profile input, #submit {
        margin: 10px auto;
    }

    #profile input, #submit {
        padding: 5px 7px;
        color: black;
    }

</style>

<div id="banner" class="banner full-screen-mode parallax">
    <div class="container pr">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="banner-static">
                <div class="banner-text">
                    <div class="banner-cell">
                        <div id="profile" class="cente">
                            <c:url value="/user/profile/update" var="action_url"/>
                            <h2 class="h4Margin">Le tue informazioni</h2>
                            <form:form id="profileForm" modelAttribute="user" method="POST" action="${action_url}" onsubmit="return continueOrNot()">
                                <div id="profile_div">

                                    <table class="h4Margin">
                                        <tr>
                                            <td><label>E-mail</label></td>
                                        </tr>
                                        <tr>
                                            <td onchange="validateEmail()"><input placeholder='E-mail' name='mailUser' id='mailUser'></td>
                                        </tr>

                                        <tr>
                                            <td>
                                                <span id="messageEmail"></span>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td><label>Username</label></td>
                                        </tr>
                                        <tr>
                                            <td><input style="text-align:center;" placeholder='Username' name='username' id='username' disabled></td>
                                        </tr>
                                    </table>
                                    <br>
                                    <table class="h4Margin">
                                        <tr>
                                            <td><label>Password</label></td>
                                            <td><label>Ripeti Password</label></td>
                                        </tr>
                                        <tr>
                                            <td onchange="validate()"><input class="tdMargin" type='password' id='password' placeholder='Password' name='password'></td>
                                            <td onchange="validate()"><input class="tdMargin" type='password' id='password2' placeholder='Ripeti Password'></td>
                                        </tr>
                                    </table>
                                    <table class="h4Margin">
                                        <tr>
                                            <td>
                                                <label id="messagePassword"></label>
                                            </td>
                                        </tr>
                                    </table>
                                    <table class="h4Margin">
                                        <tr>
                                            <td><label>Nome</label></td>
                                            <td><label>Cognome</label></td>
                                        </tr>
                                        <tr>
                                            <td><input class="tdMargin" placeholder='Nome' name='firstNameUser' id='firstNameUser'></td>
                                            <td><input class="tdMargin" placeholder='Cognome' name='lastNameUser' id='lastNameUser'></td>
                                        </tr>
                                    </table>

                                    <br>
                                    <table class="h4Margin">
                                        <tr>
                                            <td><label>Numero di telefono</label></td>
                                        </tr>
                                        <tr>
                                            <td><input placeholder='Numero di telefono' name='phoneNumberUser' id='phoneNumberUser'></td>
                                        </tr>
                                    </table>

                                    <div class="h4Margin" id='searchsub'>
                                        <input id='submit' type='submit' value='Invio'>
                                    </div>
                                </div>
                            </form:form>

                            <c:if test="${message != null}">
                                <div class="container alert alert-danger h4Margin">
                                    <h2>
                                            ${message}
                                    </h2>
                                </div>
                            </c:if>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
