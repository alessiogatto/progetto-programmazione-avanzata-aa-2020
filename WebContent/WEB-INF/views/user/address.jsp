<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page session="false" %>
<style>
    table.table td a.edit {
        color: #FFC107;
    }

    table.table td a.delete {
        color: #F44336;
    }

    .table-title h2{
        color: whitesmoke;
    }

</style>

<script> var user = ${user};  </script>

<div id="userListAddresses" class="listOrdersContainer myBackground container-fluid">
    <div class="table-wrapper table-responsive">
        <div class="table-title">
            <div class="row">
                <div class="col-sm-3">
                    <h2 style="color: white">I miei <b>Indirizzi</b></h2>
                </div>
                <div class="col-sm-9">
                    <a id="new-add-btn" onclick="addNewAddress()" class="btn btn-success" data-toggle="modal"><i class="material-icons">&#xE147;</i>
                        <span>Aggiungi nuovo indirizzo</span></a>
                </div>
            </div>
        </div>
        <form:form id="addForm" modelAttribute="newAddress" action="/delivery/user/address/add" method="POST" >
            <table class="table table-striped table-hover">
                <thead>
                <tr>
                    <th>Città</th>
                    <th>Indirizzo</th>
                    <th>Numero Civico</th>
                    <th>CAP</th>
                    <th>Tipo</th>
                    <th>Azioni</th>
                </tr>
                </thead>

                <tbody id="addressBody">

                </tbody>
            </table>
        </form:form>
    </div>
</div>
