/* -- REGISTER -- */
if (top.location.pathname === "/delivery/register") {
    $.get("/delivery/register", () => {
    })
}

/* -- MENU -- */
if (top.location.pathname === "/delivery/menu") {
    $.get("/delivery/menu", () => {

            var available = menu.filter((article) => {
                return article.availableArticle === true;
            })

            function filterMenu(menu, categoryString) {
                let category = menu.filter((menu) => {
                    return menu.typeArticle === categoryString
                });
                populateMenuForCategory(category, categoryString.toLowerCase());
            }

            function populateMenuForCategory(category, categoryString) {
                category.forEach(function (item) {
                    $("." + categoryString).append(
                        "<div class='col-lg-6 col-md-6 col-sm-12 col-xs-12 '>" +
                        "<div class='offer-item'>" +
                        "<img src='" + item.imageArticleURL + "' alt=' '" +
                        " class='img-responsive'>" +
                        "<div>" +
                        "<h3>" + item.nameArticle + "</h3>" +
                        "<p>" +
                        item.descriptionArticle +
                        "</p>" +
                        "</div>" +
                        "<span class='offer-price'>" + '€' + item.priceArticle + "</span>" +
                        "</div>" +
                        "</div>"
                    )
                })
            }

            filterMenu(available, "PIZZE");
            filterMenu(available, "BIBITE");
            filterMenu(available, "DOLCI");
            filterMenu(available, "ANTIPASTI");
        }
    )
}

/* -- USER MENU -- */
if (top.location.pathname === "/delivery/user/menu") {

    $.get("/delivery/user/menu", () => {

        var available = menu.filter((article) => {
            return article.availableArticle === true;
        })

        function filterMenu(menu, categoryString) {
            let category = menu.filter((menu) => {
                return menu.typeArticle === categoryString
            });
            populateMenuForCategory(category, categoryString.toLowerCase());
        }

        function populateMenuForCategory(category, categoryString) {
            category.forEach(function (item) {
                let itemString = (JSON.stringify(item));
                $("." + categoryString).append(
                    "<div class='col-lg-6 col-md-6 col-sm-12 col-xs-12 '>" +
                    "<div class='offer-item'>" +
                    // "<img src='https://www.goodfoodlab.it/wp-content/uploads/2020/05/48F2857A-EB73-4464-9C37-93E3418BF2EB-500x500.jpeg' alt=' '" +
                    "<img src='" + item.imageArticleURL + "' alt=' '" +
                    " class='img-responsive'>" +
                    "<div>" +
                    "<h3>" + item.nameArticle + "</h3>" +
                    "<p>" +
                    item.descriptionArticle +
                    "</p>" +
                    "</div>" +
                    "<span class='offer-price'>" + '€' + item.priceArticle + "</span>" +
                    "</div>" +
                    '<div style="margin: 0 auto" class="center">' +
                    '<p>' + item.ingredientsArticle + '</p>' +
                    '</div>' +
                    "<div class='center'>" +
                    '<label class=\'tdMargin\'>Quantità</label>' +
                    '<input type="number" class="quantita" id="article' + item.idArticle + '" value="1">' +
                    "<button value='Add-to-cart' class='btn-add-article tdMargin' onclick='addArticleToCart(" + itemString.replace("'", "&apos;") + ")'>Aggiungi</button>" +
                    '</div>' +
                    "</div>"
                )
            })
        }

        filterMenu(available, "PIZZE");
        filterMenu(available, "BIBITE");
        filterMenu(available, "DOLCI");
        filterMenu(available, "ANTIPASTI");

    })

    function addToCart(isFromDelete) {
        if (cart.length > 0) {
            let cartString = "<table class='table'> <thead> <th>Nome</th> <th>Quantità</th> <th>Prezzo</th> <th>Iva</th> <th>Elimina</th> </thead> <tbody>";
            cart.forEach(function (item) {
                cartString += '<tr>' +
                    '<td>' + item.item.nameArticle + '</td>' +
                    '<td>' + item.qnt + '</td>' +
                    '<td>' + item.item.priceArticle + '</td>' +
                    '<td>' + item.item.iva + '</td>' +
                    '<td>' + "<a id=\"delete-btn\" style=\"cursor: pointer\"" + " class='delete' data-toggle='modal' onclick='deleteCartItem(" + item.item.idArticle + ")" + "'><i class='material-icons'" +
                    " data-toggle='tooltip'" +
                    " title='Delete'> &#xE872; </i></a>" + '</td>'
                '</tr>';
            })
            cartString += '</tbody></table>';
            $("#cart").html(cartString);
        } else if (isFromDelete) {
            $("#cart").html("");
        } else alert("selezionare qualcosa!");
    }

    function addArticleToCart(item) {
        let qnt = $("#article" + item.idArticle).val();
        deleteCartItem(item.idArticle);
        cart.push({item: item, qnt: qnt});
        alert("Aggiunto " + item.nameArticle + " x" + qnt);
        addToCart(false);
    }

    function deleteCartItem(id) {
        cart = cart.filter(item => item.item.idArticle !== id);
        addToCart(true);
    }

    function sendOrder(){
        let order = {};
        order.type = $("#typeOrder").val();
        order.payMethod = $("#payMethod").val();
        order.timeDelta = $("#timeDelta").val();
        order.variation = $("#variation").val();
        order.idAddress = $("#addressId").val();
        order.cart = (cart);
        let orderJSON = JSON.stringify(order);

        $.ajax({
            url: '/delivery/user/menu/cart/checkout',
            type: 'POST',
            data: orderJSON,
            contentType: 'application/json; charset=utf-8',
            success: function (response) {
                alert('Invio effettuato');
                window.location.href = response;
                },
            error: function () {
                alert("Errore! Controlla il carrello!");
            }
        })
    }

    function completeOrder() {

        if (cart.length !== 0){
            $("#complete-order").html(
                '<div class="center" style="padding:2rem">\n' +
                '                <table class="table">\n' +
                '                    <tr>' +
                '                        <td>\n' +
                '                            <label>Tipologia di ritiro</label>\n' +
                '                        </td>\n' +
                '                        <td>\n' +
                '                            <label>Tipologia di pagamento</label>\n' +
                '                        </td>\n' +
                '                    </tr>' +
                '                    <tr>\n' +
                '                        <td>\n' +
                '                            <select name="typeOrder" id="typeOrder">\n' +
                '                                <option value="TAKEAWAY">TAKEAWAY</option>\n' +
                '                                <option value="DELIVERY">DELIVERY</option>\n' +
                '                            </select>\n' +
                '                        </td>\n' +
                '                        <td>\n' +
                '                            <select name="payMethod" id="payMethod">\n' +
                '                                <option value="CONTANTI">CONTANTI</option>\n' +
                '                                <option value="CARTA_DI_CREDITO">CARTA_DI_CREDITO</option>\n' +
                '                                <option value="PAYPAL">PAYPAL</option>\n' +
                '                            </select>\n' +
                '                        </td>\n' +
                '                    </tr>\n' +
                '               </table>' +
                '                <table class="table">\n' +
                '                    <tr>' +
                '                        <td>\n' +
                '                            <label>Indirizzo di recapito</label>\n' +
                '                        </td>\n' +
                '                    </tr>' +
                '                    <tr>\n' +
                '                        <td>\n' +
                '                            <select name="addressId" id="addressId">\n' +
                '                                <option value="">Delivery pizza</option>\n' +
                '                            </select>\n' +
                '                        </td>\n' +
                '                    </tr>\n' +
                '               </table>' +
                '                <table class="table">\n' +
                '                    <tr>' +
                '                        <td>\n' +
                '                            <label>Ritiro/consegna tra</label>\n' +
                '                        </td>\n' +
                '                    </tr>' +
                '                    <tr>\n' +
                '                        <td>\n' +
                '                            <select name="timeDelta" id="timeDelta">\n' +
                '                                <option value="1">1h</option>\n' +
                '                                <option value="2">2h</option>\n' +
                '                                <option value="3">3h</option>\n' +
                '                            </select>\n' +
                '                        </td>\n' +
                '                    </tr>\n' +
                '               </table>' +
                '                <table class="table">\n' +
                '                    <tr>' +
                '                        <td>\n' +
                '                            <label>Richieste varie</label>\n' +
                '                        </td>\n' +
                '                    </tr>' +
                '                    <tr>\n' +
                '                        <td>\n' +
                '                <textarea name="variation" id="variation" cols="20" rows="4">\n' +
                '</textarea>\n' +
                '                        </td>\n' +
                '                    </tr>\n' +
                '                </table>\n' +
                '                <button onClick="sendOrder()">Invia Ordine</button>\n' +
                '            </div>'
            )

            $('#typeOrder').change( ()=>{
                let selectedOption = $("#typeOrder option:selected")
                if (selectedOption.val() === "DELIVERY"){
                    let options = "";
                    user.addresses.forEach((address) => {
                        options += '<option value="' + address.idAddress + '">'  + address.typeAddress + ': ' + address.streetAddress + ' ' + address.numberAddress + ' ' + address.cityAddress +'</option>'
                    })
                    console.log(options)
                    $('#addressId').html(options);
                }else{
                    $('#addressId').html('<option value="">Delivery Pizza</option>');
                }
            })
        }
    }
}

/* -- ADMIN LIST_ORDERS -- */
if (top.location.pathname === "/delivery/admin/listorders") {

    function changeState(newState, orderId) {

        $.post("listordersStateModified",
            {
                changeTo: newState,
                    id: orderId,
    },
        function () {
                window.location.reload();
        }
    )

    }

    $.get("/delivery/admin/listorders", () => {

        let selectedValue = localStorage.getItem("selectedValue");
        let ordersToView = orders;
        if (selectedValue !== null) {

            if (selectedValue === "ALL") {
                ordersToView = orders
            } else {

                $("#statusFilter").val(selectedValue)

                ordersToView = orders.filter((order) => {
                    return order.stateOrder === selectedValue
                })
            }
        }

        $('#statusFilter').on('change', () => {

            const selectedElement = $("#statusFilter option:selected");
            const selectedValue = selectedElement.val();
            localStorage.setItem("selectedElement", selectedElement)
            localStorage.setItem("selectedValue", selectedValue)

            window.location.reload();
        })

        if (ordersToView.length < 5){
            $('#adminListOrder').attr( 'style', 'height: 100%')
        }

        $.each(ordersToView, (index, order) => {

            let confirm = "";
            let conclude = "";
            let cancel = "";
            let statusColor = "text-warning";
            const {
                creationDateOrder: {
                    year: cYear,
                        month: cMonth,
                        dayOfMonth: cDay,
                        hourOfDay: cHour,
                        minute: cMinute
                },
            numberOrder: numberOrder,
                payMethodOrder: payMethod,
                pickUpTimeOrder: {
                    hourOfDay: pHour,
                        minute: pMinute
            },
            stateOrder: state,
                totalAmountOrder: totalAmount,
                typeOrder: typeOrder,
                userOrder: {
                    addresses: addresses,
                        firstNameUser: firstName,
                    lastNameUser: lastName
            },
            variationRequest: requests
        } = order;
            const customer = firstName + " " + lastName;
            const address = addresses[0].streetAddress + " " + addresses[0].numberAddress + " " + addresses[0].capAddress + " " + addresses[0].cityAddress;
            const creation = cDay + "/" + (cMonth + 1) + "/" + cYear + " alle " + cHour + "." + ('0' + cMinute).slice(-2);
            const pickUp = pHour + "." + ('0' + pMinute).slice(-2);
            const total = "€ " + totalAmount;
            const cartUrl = "cart/" + numberOrder;
            const newStateConclude = "'CONCLUDED'";
            const newStateConfirm = "'CONFIRMED'";
            const newStateCancelled = "'CANCELLED'";
            if (state === 'CONCLUDED') {
                statusColor = "text-dark"
            } else if (state === 'CANCELLED') {
                statusColor = "text-danger"
            } else {
                cancel = '<a style="cursor: pointer" onclick="changeState(' + newStateCancelled + ', ' + numberOrder + ')">' +
                    "<i class='material-icons text-danger'>delete</i>" +
                    "</a>"
                ;
                if (state === 'NEW') {
                    confirm = '<a style="cursor: pointer" onclick="changeState(' + newStateConfirm + ', ' + numberOrder + ')">' +
                        "<i class='material-icons text-success'>check</i>" +
                        "</a>"
                    ;
                    statusColor = "text-warning";
                } else if (state === 'CONFIRMED') {
                    conclude = '<a style="cursor: pointer" onclick="changeState(' + newStateConclude + ', ' + numberOrder + ')">' +
                        "<i class='material-icons text-success'>done_all</i>" +
                        "</a>"
                    ;
                    statusColor = "text-success";
                }
            }

            $("#listOrders").append(
                "<tr>" +
                "<td>" + customer + "</td>" +
                "<td>" + address + "</td>" +
                "<td>" + creation + "</td>" +
                "<td>" + pickUp + "</td>" +
                "<td class='status " + statusColor + "'>" +
                state +
                "</td>" +
                "<td>" + total + "</td>" +
                "<td>" + requests + "</td>" +
                "<td>" + payMethod + "</td>" +
                "<td>" + typeOrder + "</td>" +
                "<td>" +
                "<a href='" + cartUrl + "' class='view' title='View Details' data-toggle='tooltip'>" +
                "<i class='material-icons'>&#xE5C8;</i>" +
                "</a>" +
                "</td>" +
                "<td>" +
                confirm +
                "</td>" +
                "<td>" +
                conclude +
                "</td>" +
                "<td>" +
                cancel +
                "</td>" +
                "</tr>"
            )
        })
    })
}

    /* - ADMIN/CART - */
    if (top.location.pathname.substring(0, 21) === "/delivery/admin/cart/") {

        const orderId = top.location.pathname.substring(21)

        $.get("/delivery/admin/cart/" + orderId, () => {

            if (cartDetails.length < 5){
                $('#adminCartList').attr( 'style', 'height: 100%')
            }

            $.each(cartDetails, (index, cartItem) => {
                const {
                    articleCartItem: {
                        dieta: dieta,
                        nameArticle: nameArticle
                    },
                    quantityArticle: quantity,

                } = cartItem;

                $("#cartBody").append(
                    '<tr>' +
                    '<td>' + (index + 1) + '</td>' +
                    '<td>' + nameArticle + '</td>' +
                    '<td>' + quantity + '</td>' +
                    '<td>' + dieta + '</td>' +
                    '</tr>'
                )

            })

        })
    }

// /* -PROFILE- */
    if (top.location.pathname === "/delivery/user/profile") {
        $('#mailUser').val(user["mailUser"]);
        $("#username").val(user["username"]);
        $("#password").val(user["password"]);
        $("#firstNameUser").val(user["firstNameUser"]);
        $("#lastNameUser").val(user["lastNameUser"]);
        $("#phoneNumberUser").val(user["phoneNumberUser"]);
    }

// /* -REGISTER-*/
    function validate() {
        var password1 = document.getElementById("password");
        var password2 = document.getElementById("password2");
        var msg = document.getElementById("messagePassword");
        var greenColor = "#adffad";
        var redColor = "#fc8787";

        if (password2.value === '' || password1.value === '') {
            msg.innerHTML = "Inserisci la password!";
            return false;
        } else if (password1.value === password2.value) {
            password2.style.backgroundColor = greenColor;
            msg.style.color = greenColor;
            msg.innerHTML = "Bene!";
            return true;
        } else {
            password2.style.backgroundColor = redColor;
            msg.style.color = redColor;
            msg.innerHTML = "Riprova!";
            return false;
        }
    }

    function validateEmail() {
        var mail = document.getElementById("mailUser");
        var mailformat = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        var msg = document.getElementById("messageEmail");
        var greenColor = "#adffad";
        var redColor = "#fc8787";
        if (mail.value.match(mailformat)) {
            mail.style.backgroundColor = greenColor;
            msg.style.color = greenColor;
            msg.innerHTML = "Mail corretta!";
            return true;
        } else {
            mail.style.backgroundColor = redColor;
            msg.style.color = redColor;
            msg.innerHTML = "Inserisci una mail corretta!";
            return false;
        }
    }

    $('#registerForm, #profileForm').submit(function () {
        let bool;
        if (($("#firstNameUser").val()) === "" || ($("#lastNameUser").val()) === "" || ($("#phoneNumberUser").val()) === "" || ($("#password").val()) === "" || ($("#password2").val()) === "") {
            alert('Non hai messo tutti i campi!');
            bool = false;
            return bool;
        } else
            bool = true;
        return bool;
    });

    function continueOrNot() {

        if (validateEmail() === true && validate() === true && bool === true) {
            return true;
        } else if (validateEmail() === false && validate() === true || bool === false) {
            return false;
        } else if (validateEmail() === true && validate() === false || bool === false) {
            return false;
        } else return false;

    }

    function disableOtherAction() {
        $("#new-add-btn").attr("onclick", "");
        $("#edit-btn, #delete-btn").attr("onclick", "");
    }

// SHOW_ADDRESS
    if (top.location.pathname === "/delivery/user/address") {

        let addresses = user.addresses;

        $.get("/delivery/user/address", () => {

            if (addresses.length < 5){
                $('#userListAddresses').attr( 'style', 'height: 100%')
            }

            $.each(addresses, (index, address) => {

                $("#addressBody").append(
                    '<tr>' +
                    '<td>' + address.cityAddress + '</td>' +
                    '<td>' + address.streetAddress + '</td>' +
                    '<td>' + address.numberAddress + '</td>' +
                    '<td>' + address.capAddress + '</td>' +
                    '<td>' + address.typeAddress + '</td>' +
                    '<td>' +
                    "<a id=\"edit-btn\" style=\"cursor: pointer\"" + " data-toggle='modal' onclick='updateAddress(" + address.idAddress + ")" + "'><i class='material-icons'" +
                    " data-toggle='tooltip'" +
                    "title = 'Edit' > &#xE254; " +
                    " </i></a>" +
                    "<a id=\"delete-btn\" style=\"cursor: pointer\"" + " class='delete' data-toggle='modal' onclick='deleteAddress(" + address.idAddress + ")" + "'><i class='material-icons'" +
                    " data-toggle='tooltip'" +
                    " title='Delete'> &#xE872; </i></a>" +
                    '</td>' +
                    '</tr>'
                )
            })

        })

        function updateAddress(addressId) {

            $("#addressBody").append(
                '<tr>' +
                '<td>' + '<input id="cityAddress" class="tdMargin" type="text" maxlength="25" placeholder="città" name="cityAddress">' + '</td>' +
                '<td>' + '<input id="streetAddress" class="tdMargin" type="text" maxlength="30" placeholder="Indirizzo" name="streetAddress">' + '</td>' +
                '<td>' + '<input id="numberAddress" class="tdMargin" type ="text" maxlength="10" placeholder="Numero civico" name="numberAddress">' + '</td>' +
                '<td>' + '<input id="capAddress" class="tdMargin" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"\n' +
                '    type = "number"\n' +
                '    maxlength = "5" placeholder="Cap" name="capAddress" >' + '</td>' +

                '<td>' +
                '<select name="typeAddress" id="typeAddress">' +
                '<option value="CASA">CASA</option>' +
                '<option value="PREFERITO">PREFERITO</option>' +
                '<option value="LAVORO">LAVORO</option>' +
                '</select>' +
                '</td>' +

                '<td>' + '<a style="cursor: pointer" onclick="modifyAddress(' + addressId + ')"><i class="material-icons" ></i> <span class="material-icons">\n' +
                'send\n' +
                '</span> </a>' + '</td>' +
                '</tr>'
            );

            let selectedAddress = addresses.filter((address) => {
                return address.idAddress === addressId;
            })

            $('#cityAddress').val(selectedAddress[0].cityAddress);
            $('#streetAddress').val(selectedAddress[0].streetAddress);
            $('#numberAddress').val(selectedAddress[0].numberAddress);
            $('#capAddress').val(selectedAddress[0].capAddress);
            $('#typeAddress').val(selectedAddress[0].typeAddress);


            disableOtherAction();
        }

        function modifyAddress(addressId) {

            if (($("#cityAddress").val()) === "" || ($("#streetAddress").val()) === "" || ($("#capAddress").val()) === "") {
                alert('Non hai messo tutti i campi!');
            } else {
                $.post("/delivery/user/address/update/",
                    {
                        idAddress: addressId,
                        cityAddress: document.getElementById("cityAddress").value,
                        streetAddress: document.getElementById("streetAddress").value,
                        numberAddress: document.getElementById("numberAddress").value,
                        capAddress: document.getElementById("capAddress").value,
                        typeAddress: document.getElementById("typeAddress").value
                    },
                    () => {
                        window.location.reload();
                    }
                )
            }
        }

        function addNewAddress() {
            $("#addressBody").append(
                '<tr>' +
                '<td>' + '<input id="cityAddress" class="tdMargin" type="text" placeholder="Città" name="cityAddress">' + '</td>' +
                '<td>' + '<input id="streetAddress" class="tdMargin" type="text" placeholder="Indirizzo" name="streetAddress">' + '</td>' +
                '<td>' + '<input id="numberAddress" class="tdMargin" type="text" placeholder="N." name="numberAddress">' + '</td>' +
                '<td>' + '<input id="capAddress" class="tdMargin" type="number" placeholder="Cap" name="capAddress">' + '</td>' +

                '<td>' +
                '<select name="typeAddress" id="typeAddress">' +
                '<option value="CASA">CASA</option>' +
                '<option value="PREFERITO">PREFERITO</option>' +
                '<option value="LAVORO">LAVORO</option>' +
                '</select>' +
                '</td>' +

                '<td>' + '<a style="cursor: pointer" onclick="addAddress()"><i class="material-icons" ></i> <span class="material-icons">\n' +
                'send\n' +
                '</span> </a>' + '</td>' +
                '</tr>'
            )

            disableOtherAction();
        }

        function addAddress() {
            if (($("#cityAddress").val()) === "" || ($("#streetAddress").val()) === "" || ($("#capAddress").val()) === "") {
                alert('Non hai messo tutti i campi!');
            } else {
                document.getElementById('addForm').submit();
            }
        }

        function deleteAddress(addressId) {
            $.post("/delivery/user/address/remove/",
                {
                    id: addressId
                }
            ).done(() => {
                window.location.reload();
            }).fail((response) => {
                alert(response.responseText);
            })
        }
    }

    /* -- USER - LIST_ORDERS -- */
    if (top.location.pathname === "/delivery/user/listorders") {

        $.get("/delivery/user/listorders", () => {

            let selectedValue = localStorage.getItem("selectedValue");

            let ordersToView = orders;

            if (selectedValue !== null) {

                if (selectedValue === "ALL") {
                    ordersToView = orders
                } else {

                    $("#statusFilter").val(selectedValue)

                    ordersToView = orders.filter((order) => {
                        return order.stateOrder === selectedValue
                    })
                }
            }

            $('#statusFilter').on('change', () => {

                const selectedElement = $("#statusFilter option:selected");
                const selectedValue = selectedElement.val();

                localStorage.setItem("selectedElement", selectedElement)
                localStorage.setItem("selectedValue", selectedValue)

                window.location.reload();

            })

            if (ordersToView.length < 5){
                $('#userListOrder').attr( 'style', 'height: 100%')
            }

            $.each(ordersToView, (index, order) => {

                const {
                    creationDateOrder: {
                        year: cYear,
                        month: cMonth,
                        dayOfMonth: cDay,
                        hourOfDay: cHour,
                        minute: cMinute
                    },
                    idAddress: orderAddress,
                    numberOrder: numberOrder,
                    payMethodOrder: payMethod,
                    pickUpTimeOrder: {
                        hourOfDay: pHour,
                        minute: pMinute
                    },
                    stateOrder: state,
                    totalAmountOrder: totalAmount,
                    typeOrder: typeOrder,
                    userOrder: {
                        addresses: addresses,
                    },
                    variationRequest: requests
                } = order;

                let statusColor = "text-warning";

                if (state === 'CONCLUDED') {
                    statusColor = "text-dark"
                } else if (state === "CONFIRMED"){
                    statusColor = "text-success"
                } else if(state === "CANCELLED"){
                    statusColor = "text-danger"
                }

                let address = ""

                if (typeOrder === "DELIVERY") {
                    const selectedAddress = addresses.filter((address) => {
                        return address.idAddress === orderAddress;
                    })
                    address = selectedAddress[0].streetAddress + " " + selectedAddress[0].numberAddress + " " + selectedAddress[0].capAddress + " " + selectedAddress[0].cityAddress;
                }

                const creation = cDay + "/" + (cMonth + 1) + "/" + cYear + " alle " + cHour + "." + ('0' + cMinute).slice(-2);
                const pickUp = pHour + "." + ('0' + pMinute).slice(-2);
                const total = totalAmount + "€";
                const cartUrl = "cart/" + numberOrder;


                $("#listOrders").append(
                    "<tr>" +
                    "<td>" + address + "</td>" +
                    "<td>" + creation + "</td>" +
                    "<td>" + pickUp + "</td>" +
                    "<td class='status " + statusColor + "'>" +
                    state +
                    "</td>" +
                    "<td>" + total + "</td>" +
                    "<td>" + requests + "</td>" +
                    "<td>" + payMethod + "</td>" +
                    "<td>" + typeOrder + "</td>" +
                    "<td>" +
                    "<a href='" + cartUrl + "' class='view' title='View Details' data-toggle='tooltip'>" +
                    "<i class='material-icons'>&#xE5C8;</i>" +
                    "</a>" +
                    "</td>" +
                    "</tr>"
                )
            })
        })
    }

    /* - USER/CART - */
    if (top.location.pathname.substring(0, 20) === "/delivery/user/cart/") {

        const orderId = top.location.pathname.substring(20)

        $.get("/delivery/user/cart/" + orderId, () => {

            if (cartDetails.length < 5){
                $('#userCartLIstOrder').attr( 'style', 'height: 100%')
            }

            $.each(cartDetails, (index, cartItem) => {
                const {
                    articleCartItem: {
                        dieta: dieta,
                        nameArticle: nameArticle
                    },
                    quantityArticle: quantity,

                } = cartItem;

                $("#cartBody").append(
                    '<tr>' +
                    '<td>' + (index + 1) + '</td>' +
                    '<td>' + nameArticle + '</td>' +
                    '<td>' + quantity + '</td>' +
                    '<td>' + dieta + '</td>' +
                    '</tr>'
                )

            })

        })
    }

    /* -- ADMIN MENU -- */
    if (top.location.pathname === "/delivery/admin/menu") {

        $.get("/delivery/admin/menu", () => {

            let selectedType = localStorage.getItem("selectedType");

            let articlesToView = menu;

            if (selectedType !== null) {
                if (selectedType === "ALL") {
                    articlesToView = menu;
                } else {

                    $("#typeFilter").val(selectedType)

                    articlesToView = menu.filter((article) => {
                        return article.typeArticle === selectedType
                    })
                }
            }

            $('#typeFilter').on('change', () => {

                const selectedElement = $("#typeFilter option:selected");
                const selectedType = selectedElement.val();

                localStorage.setItem("selectedType", selectedType)
                // localStorage.setItem("selectedDiet", null)

                window.location.reload();

            })

            $.each(articlesToView, (index, article) => {

                $("#menu").append(
                    "<tr>" +
                    "<td>" + article.nameArticle + "</td>" +
                    "<td>" + '€ ' + article.priceArticle + "</td>" +
                    "<td>" + article.descriptionArticle + "</td>" +
                    "<td>" + article.imageArticleURL + "</td>" +
                    "<td>" + article.ingredientsArticle + "</td>" +
                    "<td>" + article.typeArticle + "</td>" +
                    "<td>" + article.dieta + "</td>" +
                    "<td>" + article.iva + "</td>" +
                    "<td>" + article.availableArticle + "</td>" +

                    '<td>' +
                    "<a id=\"edit-btn\" style=\"cursor: pointer\"" + " data-toggle='modal' onclick='updateArticle(" + article.idArticle + ")" + "'><i class='material-icons'" +
                    " data-toggle='tooltip'" +
                    " title = 'Edit' > &#xE254; " +
                    " </i></a>" +
                    "<a id=\"delete-btn\" style=\"cursor: pointer\"" + " class='delete' data-toggle='modal' onclick='deleteArticle(" + article.idArticle + ")" + "'><i class='material-icons'" +
                    " data-toggle='tooltip'" +
                    " title='Delete'> &#xE872; </i></a>" +
                    '</td>' +

                    "</tr>"
                )
            })
        })

        function addNewArticle() {
            $("#menu").append(
                '<tr>' +
                '<td>' + '<input id="nameArticle" class="tdMargin" type="text" placeholder="Nome" name="nameArticle">' + '</td>' +
                '<td>' + '<input id="priceArticle" class="tdMargin" type="text" placeholder="Prezzo" name="priceArticle">' + '</td>' +
                '<td>' + '<input id="descriptionArticle" class="tdMargin" type="text" placeholder="Descrizione" name="descriptionArticle">' + '</td>' +
                '<td>' + '<input id="image" class="tdMargin" accept="image/jpeg, image/png" type="file" name="imageArticleURL">' + '</td>' +
                '<td>' + '<input id="ingredientsArticle" class="tdMargin" type="text" placeholder="Ingredienti" name="ingredientsArticle">' + '</td>' +

                '<td>' +
                '<select name="typeArticle" id="typeArticle">' +
                '<option value="PIZZE">PIZZA</option>' +
                '<option value="BIBITE">BIBITA</option>' +
                '<option value="DOLCI">DOLCE</option>' +
                '<option value="ANTIPASTI">ANTIPASTO</option>' +
                '</select>' +
                '</td>' +

                '<td>' +
                '<select name="dieta" id="dieta">' +
                '<option value="VEGANO">VEGANO</option>' +
                '<option value="VEGETARIANO">VEGETARIANO</option>' +
                '<option value="PER_TUTTI">PER_TUTTI</option>' +
                '</select>' +
                '</td>' +

                '<td>' +
                '<select name="iva" id="iva">' +
                '<option value="4">4</option>' +
                '<option value="10">10</option>' +
                '<option value="22">22</option>' +
                '</select>' +
                '</td>' +

                '<td>' +
                '<select name="availableArticle" id="availableArticle">' +
                '<option value="true">true</option>' +
                '<option value="false">false</option>' +
                '</select>' +
                '</td>' +

                '<td>' + '<a style="cursor: pointer" onclick="checkFieldArticle()"><i class="material-icons" ></i> <span class="material-icons">\n' +
                'send\n' +
                '</span> </a>' + '</td>' +
                '</tr>'
            )

            // disableOtherAction();
        }

        function checkFieldArticle(id) {
            if (((($("#nameArticle").val()) === "" || ($("#nameArticle" + id).val()) === "") || (($("#priceArticle").val()) === "" || ($("#priceArticle" + id).val()) === "") || (($("#descriptionArticle").val()) === "" || ($("#descriptionArticle" + id).val()) === ""))) {
                alert('Non hai messo tutti i campi!');
            } else {
                var fd = new FormData();
                if (id === undefined) {
                    var files = $('#image')[0].files;
                } else {
                    var files = $('#image' + id)[0].files;
                }
                // Check file selected or not
                if (files.length > 0) {
                    fd.append('image', files[0]);
                    $.ajax({
                        url: '/delivery/admin/menu/addImage',
                        type: 'POST',
                        data: fd,
                        contentType: false,
                        enctype: 'multipart/form-data',
                        processData: false,
                        success: function () {
                            alert('Immagine caricata');
                        },
                        error: function (response) {
                            alert(response.responseText);
                        }
                    })
                }

                if (id === undefined) {
                    document.getElementById("addForm").submit();
                } else {
                    sendUpdateArticle(id)
                }
            }
        }

        function updateArticle(articleId) {

            $("#menu").append(
                '<tr>' +
                '<td>' + '<input id="nameArticle' + articleId + '" class="tdMargin" type="text" placeholder="Nome" name="nameArticle">' + '</td>' +
                '<td>' + '<input id="priceArticle' + articleId + '" class="tdMargin" type="text" placeholder="Prezzo" name="priceArticle">' + '</td>' +
                '<td>' + '<input id="descriptionArticle' + articleId + '" class="tdMargin" type="text" placeholder="Descrizione" name="descriptionArticle">' + '</td>' +
                '<td>' + '<input id="image' + articleId + '" class="tdMargin" accept="image/jpeg, image/png" type="file" name="imageArticleURL">' + '</td>' +
                '<td>' + '<input id="ingredientsArticle' + articleId + '" class="tdMargin" type="text" placeholder="Ingredienti" name="ingredientsArticle">' + '</td>' +

                '<td>' +
                '<select name="typeArticle" id="typeArticle' + articleId + '">' +
                '<option value="PIZZE">PIZZA</option>' +
                '<option value="BIBITE">BIBITA</option>' +
                '<option value="DOLCI">DOLCE</option>' +
                '<option value="ANTIPASTI">ANTIPASTO</option>' +
                '</select>' +
                '</td>' +

                '<td>' +
                '<select name="dieta" id="dieta' + articleId + '">' +
                '<option value="VEGANO">VEGANO</option>' +
                '<option value="VEGETARIANO">VEGETARIANO</option>' +
                '<option value="PER_TUTTI">PER_TUTTI</option>' +
                '</select>' +
                '</td>' +

                '<td>' +
                '<select name="iva" id="iva' + articleId + '">' +
                '<option value="4">4</option>' +
                '<option value="10">10</option>' +
                '<option value="22">22</option>' +
                '</select>' +
                '</td>' +

                '<td>' +
                '<select name="availableArticle" id="availableArticle' + articleId + '">' +
                '<option value="true">true</option>' +
                '<option value="false">false</option>' +
                '</select>' +
                '</td>' +

                '<td>' + '<a style="cursor: pointer" onclick="checkFieldArticle(' + articleId + ')"><i class="material-icons" ></i> <span class="material-icons">\n' +
                'send\n' +
                '</span> </a>' + '</td>' +
                '</tr>'
            );
            let selectedArticle;
            $.each(menu, (index, article) => {
                if (article.idArticle === articleId) {
                    selectedArticle = article;
                }
            });
            console.log(selectedArticle["imageArticleURL"])
            $('#nameArticle' + articleId).val(selectedArticle["nameArticle"]);
            $('#priceArticle' + articleId).val(selectedArticle["priceArticle"]);
            $('#descriptionArticle' + articleId).val(selectedArticle["descriptionArticle"]);
            // $('#image' + articleId).val(selectedArticle["imageArticleURL"]);
            $('#ingredientsArticle' + articleId).val(selectedArticle["ingredientsArticle"]);
            $('#dieta' + articleId).val(selectedArticle["dieta"]);
            $('#typeArticle' + articleId).val(selectedArticle["typeArticle"]);
            $('#iva' + articleId).val(selectedArticle["iva"]);
            $('#availableArticle' + articleId).val(selectedArticle["availableArticle"].toString());


            disableOtherAction();
        }

        function sendUpdateArticle(articleId) {
            console.log(document.getElementById("image" + articleId).value)

            if (($('#nameArticle' + articleId).val()) === "" || ($('#priceArticle' + articleId).val()) === "" || ($('#descriptionArticle' + articleId).val()) === "") {
                alert('Non hai messo tutti i campi!');
            } else {

                var img = "";
                if (document.getElementById("image" + articleId).value !== "") {
                    img = document.getElementById("image" + articleId).files[0].name;
                }
                console.log(document.getElementById("image" + articleId))


                $.post("/delivery/admin/menu/update/",
                    {
                        idArticle: articleId,
                        nameArticle: document.getElementById("nameArticle" + articleId).value,
                        priceArticle: document.getElementById("priceArticle" + articleId).value,
                        ingredientsArticle: document.getElementById("ingredientsArticle" + articleId).value,
                        descriptionArticle: document.getElementById("descriptionArticle" + articleId).value,
                        imageArticleURL: img,
                        typeArticle: document.getElementById("typeArticle" + articleId).value,
                        dieta: document.getElementById("dieta" + articleId).value,
                        iva: document.getElementById("iva" + articleId).value,
                        availableArticle: document.getElementById("availableArticle" + articleId).value
                    },
                    () => {
                        window.location.reload();
                    }
                )
            }
        }

        function deleteArticle(articleId) {
            $.post("/delivery/admin/menu/remove/",
                {
                    id: articleId
                }
            ).done(() => {
                window.location.reload();
            }).fail((response) => {
                alert(response.responseText);
            })
        }

    }


